@echo off

pushd %~dp0

set code_path=../code/

if not exist "../build/" mkdir "../build/"
pushd "../build/"

if "%1"=="msvc" goto msvc_compiler

echo Compiling with Clang...

clang -ogl_function_builder.exe -O0 -std=c11 -Weverything -Werror %code_path%gl_function_builder.c

gl_function_builder.exe > %code_path%gl_function_loader.c

if not exist gl_function_builder.exe goto builder_failed_error

set disabled_warnings=-Wno-newline-eof
set disabled_warnings=%disabled_warnings% -Wno-float-equal
set disabled_warnings=%disabled_warnings% -Wno-extra-semi-stmt

set disabled_errors=-Wno-format-nonliteral
set disabled_errors=%disabled_errors% -Wno-switch-enum


set debug_ignore_warnings=-Wno-unused-function
set debug_ignore_warnings=%debug_ignore_warnings% -Wno-unused-parameter
set debug_ignore_warnings=%debug_ignore_warnings% -Wno-unused-macros
set debug_ignore_warnings=%debug_ignore_warnings% -Wno-unused-variable

clang -owin32_rtiow.exe -std=c11 -g -I../libs/ -Weverything -Werror %debug_ignore_warnings% %disabled_warnings% %disabled_errors% %code_path%win32_rtiow.c -lUser32.lib -lGdi32.lib -lOpenGL32.lib

goto build_finished

:msvc_compiler

echo Compiling with MSVC

cl /nologo /O2 /Wall /WX %code_path%gl_function_builder.c

gl_function_builder.exe > %code_path%gl_function_loader.c

if not exist gl_function_builder.exe goto builder_failed_error

rem enumerator in switch is not handled
set disabled_warnings=/wd4061
rem set disabled_warnings=%disabled_warnings% -Wno-float-equal

set disabled_errors=
rem set disabled_errors=-Wno-format-nonliteral
rem set disabled_errors=%disabled_errors% -Wno-switch-enum

rem byte padding added after data member
set debug_ignore_warnings=/wd4820
rem unreferenced parameter
set debug_ignore_warnings=%debug_ignore_warnings% /wd4100
rem local variable initialized but not referenced
set debug_ignore_warnings=%debug_ignore_warnings% /wd4189
rem Compiler will insert Spectre mitigation if /Qspectre is specified
set debug_ignore_warnings=%debug_ignore_warnings% /wd5045
rem set debug_ignore_warnings=%debug_ignore_warnings% -Wno-unused-variable

cl /nologo /TC /I../libs/ /std:c11 /Od /Wall /WX /Zi %debug_ignore_warnings% %disabled_warnings% %disabled_errors% %code_path%win32_rtiow.c /link User32.lib Gdi32.lib OpenGL32.lib

goto build_finished

:builder_failed_error

echo ERROR: Failed to build gl_function_builder.exe

:build_finished

echo Done

popd
