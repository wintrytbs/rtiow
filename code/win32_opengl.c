#define MAX_SHADER_UNIFORM_COUNT 20
typedef struct shader_meta_info
{
    char *filename;
    GLuint programID;
    bool active;
    FILETIME lastModified;

    bool uniformsLoaded;
    GLint uniforms[MAX_SHADER_UNIFORM_COUNT];

} shader_meta_info;

typedef enum shader_id
{
    ShaderID_RTX,
    ShaderID_Noise,
    ShaderID_TexturedRect,
    ShaderID_Count,
} shader_id;

typedef enum rtx_shader_uniform
{
    RTXShaderUniform_iTime,
    RTXShaderUniform_process_mode,
    RTXShaderUniform_current_sample_index,
    RTXShaderUniform_cam_pos,
    RTXShaderUniform_cam_aperture,
    RTXShaderUniform_view,
    RTXShaderUniform_use_motion_blur,
    RTXShaderUniform_use_sky_background,
    RTXShaderUniform_image_plane,

    RTXShaderUniform_Count,
} rtx_shader_uniform;

typedef enum noise_shader_uniform
{
    NoiseShaderUniform_iTime,

    NoiseShaderUniform_Count,
} noise_shader_uniform;

typedef enum textured_rect_shader_uniform
{
    TexturedRectShaderUniform_tex,

    TexturedRectShaderUniform_Count,
} textured_rect_shader_uniform;

typedef struct shader_source
{
    u8 *vertSrc;
    u8 *fragSrc;
    i32 vertLength;
    i32 fragLength;

} shader_source;

internal GLuint OpenGLCreateShaderStorageBuffer(size_t byteSize, GLenum usage, GLuint bindingPoint, void *data)
{
    // TODO(torgrim): Check that the size is reasonable here.
    GLuint bufferID;
    glGenBuffers(1, &bufferID);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, bufferID);
    glBufferData(GL_SHADER_STORAGE_BUFFER, (GLsizeiptr)byteSize, data, usage);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, bindingPoint, bufferID);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    return bufferID;
}

internal void OpenGLFlushErrors(void)
{
    while(glGetError() != GL_NO_ERROR);
}

internal void OpenGLCatchError(void)
{
    GLenum err;
    while((err = glGetError()) != GL_NO_ERROR)
    {
        switch(err)
        {
            case GL_INVALID_ENUM:
            {
                PrintDebugText("Invalid Enum\n");
            }break;
            case GL_INVALID_VALUE:
            {
                PrintDebugText("Invalid Value\n");
            }break;
            case GL_INVALID_OPERATION:
            {
                PrintDebugText("Invalid Operation\n");
            }break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
            {
                PrintDebugText("Invalid Framebuffer Operation\n");
            }break;
            case GL_OUT_OF_MEMORY:
            {
                PrintDebugText("Out of Memory\n");
            }break;
            case GL_STACK_UNDERFLOW:
            {
                PrintDebugText("Stack Undeflow\n");
            }break;
            case GL_STACK_OVERFLOW:
            {
                PrintDebugText("Stack Overflow\n");
            }break;
        }

        tbs_assert(false);
    }

}

internal bool OpenGLCheckFramebufferStatus(GLuint bufferID, GLenum target)
{
    GLint drawBuffer;
    GLint readBuffer;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &drawBuffer);
    glGetIntegerv(GL_READ_FRAMEBUFFER_BINDING, &readBuffer);

    glBindFramebuffer(target, bufferID);

    GLenum fboCheck = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    bool result = (fboCheck == GL_FRAMEBUFFER_COMPLETE);
    if(!result)
    {
        switch(fboCheck)
        {
            case GL_FRAMEBUFFER_UNDEFINED:
            {
                PrintDebugText("GL_ERROR::framebuffer undefined\n");
            } break;
            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            {
                PrintDebugText("GL_ERROR::framebuffer incomplete attachment\n");
            } break;
            case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            {
                PrintDebugText("GL_ERROR::framebuffer missing attachment\n");
            } break;
            case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
            {
                PrintDebugText("GL_ERROR::framebuffer incomplete draw buffer\n");
            } break;
            case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
            {
                PrintDebugText("GL_ERROR::framebuffer incomplete read buffer\n");
            } break;
            case GL_FRAMEBUFFER_UNSUPPORTED:
            {
                PrintDebugText("GL_ERROR::framebuffer unsupported\n");
            } break;
            case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
            {
                PrintDebugText("GL_ERROR::framebuffer incomplete multisample\n");
            } break;
            case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
            {
                PrintDebugText("GL_ERROR::framebuffer incomplete layer targets\n");
            } break;
            default:
            {
                PrintDebugText("GL_ERROR::framebuffer unknown error\n");
            } break;
        }

        tbs_assert(false);
    }

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, (GLuint)drawBuffer);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, (GLuint)readBuffer);

    return result;
}

internal shader_source OpenGLParseShaderSource(u8 *src, i32 length)
{
    shader_source result = {0};
    //u8 *vertToken = (u8 *)"//@tbs_vert";
    u8 *fragToken = (u8 *)"//@tbs_frag";

    size_t fragL = strlen((char *)fragToken);

    tbs_assert(length != 0);
    result.vertSrc = src;
    u8 *cur = src;
    while(*cur != '\0')
    {
        line_info line = GetNextLine(cur);
        if(LineEqual(line, fragToken, fragL))
        {
            result.vertLength = (i32)(cur - result.vertSrc);
            result.fragLength = length - result.vertLength - (i32)line.length;
            result.fragSrc = cur + line.length;
            break;
        }

        cur += line.length;
        while(*cur == '\r' || *cur == '\n')
        {
            ++cur;
        }
    }

    tbs_assert(result.vertLength > 0);
    tbs_assert(result.fragLength > 0);
    tbs_assert(result.fragSrc != 0 && result.fragSrc[0] != '\0');

    return result;
}

internal void OpenGLCompileShader(GLuint shaderID, const char *src, i32 *length)
{
    glShaderSource(shaderID, 1, &src, length);
    glCompileShader(shaderID);

    GLint compileResult;
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &compileResult);
    if(compileResult == GL_FALSE)
    {
        // TODO(torgrim): Also pass the shader name here so that we can easily see
        // which shader it was.
        GLsizei logLength;
        GLchar logBuffer[512];
        glGetShaderInfoLog(shaderID, 512, &logLength, logBuffer);
        PrintDebugText("ERROR::SHADER_COMPILE:: could not compile shader\n");
        PrintDebugText(logBuffer);

        tbs_assert(false);
    }
}

internal void OpenGLLinkProgram(GLuint programID)
{
    glLinkProgram(programID);
    GLint linkStatus;
    glGetProgramiv(programID, GL_LINK_STATUS, &linkStatus);
    if(linkStatus == GL_FALSE)
    {
        GLsizei logLength;
        GLchar logBuffer[512];
        glGetProgramInfoLog(programID, 512, &logLength, logBuffer);
        PrintDebugText("ERROR::SHADER_LINK:: could not link program\n");
        PrintDebugText(logBuffer);

        tbs_assert(false);
    }
}

internal GLuint OpenGLCreateProgram(const char *vertSrc, i32 vertLength, const char *fragSrc, i32 fragLength)
{
    GLuint vertShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    OpenGLCompileShader(vertShaderID, vertSrc, &vertLength); 
    OpenGLCompileShader(fragShaderID, fragSrc, &fragLength); 

    GLuint programID = glCreateProgram();
    glAttachShader(programID, vertShaderID);
    glAttachShader(programID, fragShaderID);

    OpenGLLinkProgram(programID);

    return programID;
}

internal void OpenGLCreateProgramFromSingleFile(const char *filename, shader_meta_info *result)
{

    file_data shaderFile = Win32ReadAllText(filename);
    tbs_assert(shaderFile.length != 0);
    shader_source src = OpenGLParseShaderSource(shaderFile.content, (i32)shaderFile.length);

    memset(result, 0, sizeof(shader_meta_info));
    result->programID = OpenGLCreateProgram((char *)src.vertSrc, src.vertLength, (char *)src.fragSrc, src.fragLength);
    result->active = true;
    result->filename = tbs_CopyString(filename);
    result->lastModified = Win32GetLastWriteTime(filename);

    Win32FreeFileInfo(&shaderFile);
}

// TODO(torgrim): Better error handling.
internal void OpenGLCreateComputeProgram(char *filename, shader_meta_info *result)
{

    file_data computeSourceFile = Win32ReadAllText(filename);
    tbs_assert(computeSourceFile.length > 0);
    GLuint computeShaderID = glCreateShader(GL_COMPUTE_SHADER);
    OpenGLCompileShader(computeShaderID, (char *)computeSourceFile.content, NULL);
    GLuint progID = glCreateProgram();
    glAttachShader(progID, computeShaderID);
    OpenGLLinkProgram(progID);
    tbs_assert(progID > 0);

    memset(result, 0, sizeof(shader_meta_info));
    result->active = true;
    result->filename = tbs_CopyString(filename);
    result->lastModified = Win32GetLastWriteTime(filename);
    result->programID = progID;
}

internal void ReloadShaderFromSrc(GLuint progID, GLuint vertID, GLuint fragID, const char *filename)
{
    glDetachShader(progID, vertID);
    glDetachShader(progID, fragID);

    file_data shaderFile = Win32ReadAllText(filename);
    shader_source src = OpenGLParseShaderSource(shaderFile.content, (i32)shaderFile.length);
    OpenGLCompileShader(vertID, (char *)src.vertSrc, &src.vertLength);
    OpenGLCompileShader(fragID, (char *)src.fragSrc, &src.fragLength);
    glAttachShader(progID, vertID);
    glAttachShader(progID, fragID);
    OpenGLLinkProgram(progID);

    Win32FreeFileInfo(&shaderFile);
}

internal void CheckAndReloadModifiedShaders(shader_meta_info *shaders, u32 count)
{
    for(u32 i = 0; i < count; ++i)
    {
        shader_meta_info *s = shaders + i;
        if(s->active)
        {
            FILETIME lastWrite = Win32GetLastWriteTime(s->filename);
            if(CompareFileTime(&s->lastModified, &lastWrite) == -1)
            {
                GLsizei attachedCount;
                GLuint attachedList[2];
                glGetAttachedShaders(s->programID, 2, &attachedCount, attachedList);

                if(attachedCount == 2)
                {
                    GLint type;
                    glGetShaderiv(attachedList[0], GL_SHADER_TYPE, &type);
                    GLuint vertShader = 0;
                    GLuint fragShader = 0;
                    if(type == GL_VERTEX_SHADER)
                    {
                        glGetShaderiv(attachedList[1], GL_SHADER_TYPE, &type);
                        tbs_assert(type == GL_FRAGMENT_SHADER);
                        vertShader = attachedList[0];
                        fragShader = attachedList[1];
                    }
                    else if(type == GL_FRAGMENT_SHADER)
                    {
                        glGetShaderiv(attachedList[1], GL_SHADER_TYPE, &type);
                        tbs_assert(type == GL_VERTEX_SHADER);
                        fragShader = attachedList[0];
                        vertShader = attachedList[1];
                    }

                    tbs_assert(vertShader != 0 && fragShader != 0);
                    ReloadShaderFromSrc(s->programID, vertShader, fragShader, s->filename);
                    s->uniformsLoaded = false;
                }
                else if(attachedCount == 1)
                {
                    GLint type;
                    glGetShaderiv(attachedList[0], GL_SHADER_TYPE, &type);
                    tbs_assert(type == GL_COMPUTE_SHADER);

                    file_data computeSourceFile = Win32ReadAllText(s->filename);

                    GLuint computeShader = attachedList[0];
                    glDetachShader(s->programID, computeShader);
                    OpenGLCompileShader(computeShader, (char *)computeSourceFile.content, NULL);
                    glAttachShader(s->programID, computeShader);
                    OpenGLLinkProgram(s->programID);

                    s->uniformsLoaded = false;
                    s->lastModified = lastWrite;
                }
                else
                {
                    tbs_assert(false);
                }
            }
        }
    }
}

internal void OpenGLLoadUniforms(shader_meta_info *shader, shader_id shaderID)
{
    switch(shaderID)
    {
        case ShaderID_RTX:
        {
            GLint count;
            glGetProgramiv(shader->programID, GL_ACTIVE_UNIFORMS, &count);
            tbs_assert(count <= MAX_SHADER_UNIFORM_COUNT);
            tbs_assert(count <= RTXShaderUniform_Count);

            shader->uniforms[RTXShaderUniform_iTime]                = glGetUniformLocation(shader->programID, "iTime");
            shader->uniforms[RTXShaderUniform_process_mode]         = glGetUniformLocation(shader->programID, "process_mode");
            shader->uniforms[RTXShaderUniform_current_sample_index] = glGetUniformLocation(shader->programID, "current_sample_index");
            shader->uniforms[RTXShaderUniform_cam_aperture]         = glGetUniformLocation(shader->programID, "cam_aperture");
            shader->uniforms[RTXShaderUniform_cam_pos]              = glGetUniformLocation(shader->programID, "cam_pos");
            shader->uniforms[RTXShaderUniform_view]                 = glGetUniformLocation(shader->programID, "view");
            shader->uniforms[RTXShaderUniform_use_motion_blur]      = glGetUniformLocation(shader->programID, "use_motion_blur");
            shader->uniforms[RTXShaderUniform_use_sky_background]   = glGetUniformLocation(shader->programID, "use_sky_background");
            shader->uniforms[RTXShaderUniform_image_plane]          = glGetUniformLocation(shader->programID, "image_plane");

            shader->uniformsLoaded = true;
        } break;
        case ShaderID_Noise:
        {
            GLint count;
            glGetProgramiv(shader->programID, GL_ACTIVE_UNIFORMS, &count);
            tbs_assert(count <= MAX_SHADER_UNIFORM_COUNT);
            tbs_assert(count <= NoiseShaderUniform_Count);

            shader->uniforms[NoiseShaderUniform_iTime] = glGetUniformLocation(shader->programID, "iTime");

            shader->uniformsLoaded = true;
        } break;
        case ShaderID_TexturedRect:
        {
            GLint count;
            glGetProgramiv(shader->programID, GL_ACTIVE_UNIFORMS, &count);
            tbs_assert(count <= MAX_SHADER_UNIFORM_COUNT);
            tbs_assert(count <= TexturedRectShaderUniform_Count);

            shader->uniforms[TexturedRectShaderUniform_tex] = glGetUniformLocation(shader->programID, "tex");

            shader->uniformsLoaded = true;
        } break;
        default:
        {
            // NOTE: Unknown shader
            tbs_assert(false);
        }
    }
}

internal shader_meta_info *OpenGLInitShaders(void)
{
    shader_meta_info *shaderList = AllocArray(shader_meta_info, ShaderID_Count);
    OpenGLCreateComputeProgram("../code/rtiow_compute.glsl", shaderList + ShaderID_RTX);
    OpenGLCreateComputeProgram("../code/noise_compute.glsl", shaderList + ShaderID_Noise);
    OpenGLCreateProgramFromSingleFile("../code/texture_rect.glsl", shaderList + ShaderID_TexturedRect);


    OpenGLLoadUniforms(shaderList + ShaderID_RTX, ShaderID_RTX);
    OpenGLLoadUniforms(shaderList + ShaderID_Noise, ShaderID_Noise);
    OpenGLLoadUniforms(shaderList + ShaderID_TexturedRect, ShaderID_TexturedRect);

    return shaderList;
}

internal HWND Win32InitOpenGL(HINSTANCE instance, WNDPROC windowMsgProc, i32 w, i32 h, i32 majorVersion, i32 minorVersion)
{
    // TODO(torgrim): not sure we actually need to create a dummy windows,
    // only a dummy context?
    WNDCLASSA dummyClass = {0};
    dummyClass.style = CS_OWNDC;
    dummyClass.lpfnWndProc = DefWindowProc;
    dummyClass.hInstance = instance;
    dummyClass.lpszClassName = "dummy";

    if(RegisterClass(&dummyClass) == 0)
    {
        PrintDebugText("ERROR::WIN32:: could not register dummy class");
        return NULL;
    }

    HWND dummyWindowHandle = CreateWindowExA(0, "dummy", NULL, 0,
                                             50, 50,
                                             w, h,
                                             NULL, NULL,
                                             instance, NULL);
    if(dummyWindowHandle == NULL)
    {
        PrintDebugText("ERROR::WIN32:: could not create dummy window");
        return NULL;
    }

    HDC dummyDC = GetDC(dummyWindowHandle);

    PIXELFORMATDESCRIPTOR dummy_ppfd = {0};
    dummy_ppfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    dummy_ppfd.nVersion = 1;
    dummy_ppfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    dummy_ppfd.iPixelType = PFD_TYPE_RGBA;

    int dummyFormatIndex = ChoosePixelFormat(dummyDC, &dummy_ppfd);

    if(dummyFormatIndex == 0)
    {
        PrintDebugText("ERROR::WIN32:: could not choose pixel format");
    }

    if(SetPixelFormat(dummyDC, dummyFormatIndex, &dummy_ppfd) == FALSE)
    {
        PrintDebugText("ERROR::WIN32:: could not set pixel format");
        return NULL;
    }

    HGLRC dummyRC = wglCreateContext(dummyDC);
    if(wglMakeCurrent(dummyDC, dummyRC) == FALSE)
    {
        PrintDebugText("ERROR::WGL:: could not make current context");
        return NULL;
    }


    const char *className = "opengl_main_window";
    const char *windowTitle = "";
    WNDCLASSA windowClass = {0};
    windowClass.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = windowMsgProc;
    windowClass.hInstance = instance;
    windowClass.lpszClassName = className;

    if(RegisterClass(&windowClass) == 0)
    {
        PrintDebugText("ERROR::WIN32:: could not register class");
        return NULL;
    }

    HWND windowHandle = CreateWindowExA(0, className, windowTitle,
                                        WS_VISIBLE | WS_OVERLAPPEDWINDOW,
                                        50, 50,
                                        w, h,
                                        NULL, NULL,
                                        instance, NULL);
    if(windowHandle == NULL)
    {
        PrintDebugText("ERROR::WIN32:: could not create window");
        return NULL;
    }

    HDC deviceContext = GetDC(windowHandle);

    LoadGLFunctions();

    const i32 pixelFormatAttribList[] =
    {
        WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
        WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
        WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
        WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
        WGL_COLOR_BITS_ARB, 32,
        WGL_DEPTH_BITS_ARB, 8,
        0,
    };

    UINT matchingFormats;
    i32 formatIndex;
    BOOL gotWGLFormat = wglChoosePixelFormatARB(deviceContext,
                                                pixelFormatAttribList,
                                                NULL,
                                                1,
                                                &formatIndex,
                                                &matchingFormats);
    if(gotWGLFormat == FALSE)
    {
        PrintDebugText("ERROR::WGL:: could not choose pixel format");
        return NULL;
    }


    PIXELFORMATDESCRIPTOR ppfd = {0};
    if(DescribePixelFormat(deviceContext, formatIndex, sizeof(ppfd), &ppfd) == 0)
    {
        PrintDebugText("ERROR::WIN32:: could not describe pixel format");
        return NULL;
    }

    if(SetPixelFormat(deviceContext, formatIndex, &ppfd) == FALSE)
    {
        PrintDebugText("ERROR::WIN32:: could not set pixel format");
        return NULL;
    }

    const i32 contextAttribList[] =
    {
        WGL_CONTEXT_MAJOR_VERSION_ARB, majorVersion,
        WGL_CONTEXT_MINOR_VERSION_ARB, minorVersion,
        WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
        0,
    };

    HGLRC renderContext = wglCreateContextAttribsARB(deviceContext, 0, contextAttribList);

    if(wglMakeCurrent(deviceContext, renderContext) == FALSE)
    {
        PrintDebugText("ERROR::WGL:: could not make current context");
        return NULL;
    }

    if(wglDeleteContext(dummyRC) == FALSE)
    {
        PrintDebugText("ERROR::WGL:: could not delete dummy context");
        return NULL;
    }

    if(DeleteDC(dummyDC) == FALSE)
    {
        PrintDebugText("ERROR::WIN32:: could not delete dummy dc");
        return NULL;
    }

    if(DestroyWindow(dummyWindowHandle) == FALSE)
    {
        PrintDebugText("ERROR::WIN32:: could not destroy dummy window");
        return NULL;
    }

    if(UnregisterClass("dummy", instance) == FALSE)
    {
        PrintDebugText("ERROR::WIN32:: could not unregister dummy class");
        return NULL;
    }

    LoadGLFunctions();

    return windowHandle;
}
