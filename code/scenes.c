#include "scenes.h"

global const f32 defaultCameraSpeed = 20.0f;

internal scene_data CreateStairsScene(u32 *rng)
{

    scene_data result = {0};
    result.use_sky_background = true;
    result.cam.position = V3(4302.0f, 3575.0f, 997.0f);
    result.cam.rotation = V3(-24.0f, 60.0f, 0.0f);
    result.cam.fov = 40.0f;
    result.cam.aperture = 0.0f;
    result.cam.focusDist = 1.0f;
    result.cam.speed = defaultCameraSpeed;

    // Rects
    size_t boxCount = 20 * 20 * 2;
    size_t rectCount = 2 + (boxCount * 6);
    size_t rectAlloc = sizeof(rect_data) * rectCount;
    result.rects.data = malloc(rectAlloc);
    memset(result.rects.data, 0, rectAlloc);
    rect_list *rects = &result.rects;

    rects->data[rects->count].v0 = V4(-1000.0f, 0.0f, 250.0f, 0.0f);
    rects->data[rects->count].v1 = V4(4000.0f, 0.0f, 250.0f, 0.0f);
    rects->data[rects->count].v2 = V4(-1000.0f, 0.0f, -20000.0f, 0.0f);
    rects->data[rects->count].material_type = M_LAMBERTIAN;
    rects->data[rects->count].base_color = V4_1(0.3f);
    rects->data[rects->count].emit = V3_1(0.0f);
    rects->data[rects->count].ref_index = 1.5f;
    rects->data[rects->count].id = (i32)rects->count;
    rects->count++;

    for(i32 i = 0; i < 10; ++i)
    {
        AddAABox(rects->data + rects->count, (v3){0.0f, 0.0f, -50.0f * (f32)i}, 500.0f, 50.0f * (f32)(i+1), 50.0f, false, (i32)rects->count, M_LAMBERTIAN, (v3){0.0f, 0.0f, 0.0f}, false);
        rects->count += 6;
    }

    AddAABox(rects->data + rects->count, (v3){0.0f, 0.0f, -500.0f}, 500.0f, 500.0f, 1000.0f, false, (i32)rects->count, M_LAMBERTIAN, (v3){0.0f, 0.0f, 0.0f}, false);
    rects->count += 6;

    AddAABox(rects->data + rects->count, (v3){-250.0f, 0.0f, -1500.0f}, 1000.0f, 500.0f, 1000.0f, false, (i32)rects->count, M_LAMBERTIAN, (v3){0.0f, 0.0f, 0.0f}, false);
    rects->count += 6;
    for(i32 i = 0; i < 20; ++i)
    {
        AddAABox(rects->data + rects->count, (v3){-250.0f, 500.0f + (100.0f * (f32)i), -1500.0f}, 1000.0f, 100.0f, 1000.0f, false, (i32)rects->count, M_METAL, (v3){0.0f, 0.0f, 0.0f}, false);
        RotateAABox(rects->data + rects->count, (v3){0.0f, 5.0f * (f32)(i+1), 0.0f});
        rects->count += 6;
    }


    // Spheres
    size_t sphereCount = 1 + (14*14);
    size_t sphereAlloc = sizeof(sphere) * sphereCount;
    result.spheres.data = malloc(sphereAlloc);
    memset(result.spheres.data, 0, sphereAlloc);
    sphere_list *spheres = &result.spheres;

    spheres->data[spheres->count].pos = V4(250.0f, 2800.0f, -2000.0f, 0.0f);
    spheres->data[spheres->count].start_pos = spheres->data[spheres->count].pos;
    spheres->data[spheres->count].end_pos = spheres->data[spheres->count].pos;
    spheres->data[spheres->count].end_pos.y += 1500.0f;
    spheres->data[spheres->count].radius = 200.0f;
    spheres->data[spheres->count].material_type = M_LAMBERTIAN;

    float randomRed = 0.5f * (1.0f + GetRandom01(rng));
    float randomGreen = 0.5f * (1.0f + GetRandom01(rng));
    float randomBlue = 0.5f * (1.0f + GetRandom01(rng));
    spheres->data[spheres->count].base_color = V4(randomRed, randomGreen, randomBlue, 0.0f);
    spheres->data[spheres->count].emit = V4(0.0f, 1.2f, 1.2f, 0.0f);
    spheres->data[spheres->count].id = (i32)spheres->count;
    spheres->count++;


    spheres->data[spheres->count].pos = V4(250.0f, 10000.0f, 5000.0f, 0.0f);
    spheres->data[spheres->count].start_pos = spheres->data[spheres->count].pos;
    spheres->data[spheres->count].end_pos = spheres->data[spheres->count].pos;
    spheres->data[spheres->count].end_pos.y += 1500.0f;
    spheres->data[spheres->count].radius = 1000.0f;
    spheres->data[spheres->count].material_type = M_LAMBERTIAN;
    spheres->data[spheres->count].base_color = V4_1(0.0f);
    spheres->data[spheres->count].emit = V4(4.0f, 4.0f, 4.0f, 0.0f);
    spheres->data[spheres->count].id = (i32)spheres->count;
    spheres->count++;

    return result;
}

internal scene_data CreatePixelMarioScene(void)
{
    i32 isVisible[16][12] = 
    {
        {1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1},
        {0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0},
        {0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
        {0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0},
        {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
        {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
        {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
        {0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
        {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
        {0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0},
    };

    v4 colors[] =
    {
        // brown
        {68.0f/255.0f, 33.0f/255.0f, 1.0f/255.0f, 0.0f},
        // blue
        {0.0f, 0.0f, 255.0f/255.0f, 0.0f},
        // skin
        {249.0f/255.0f, 169.0f/255.0f, 110.0f/255.0f, 0.0f},
        // yellow
        {255.0f/255.0f, 255.0f/255.0f, 0.0f, 0.0f},
        // red
        {255.0f/255.0f, 0.0f, 0.0f, 0.0f},
        // black
        {0.0f, 0.0f, 0.0f, 0.0f},
    };
    i32 colorMap[16][12] =
    {
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0},
        {2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2},
        {2, 2, 2, 1, 1, 1, 1, 1, 1, 2, 2, 2},
        {2, 2, 4, 1, 3, 1, 1, 3, 1, 4, 2, 2},
        {4, 4, 4, 4, 1, 1, 1, 1, 4, 4, 4, 4},
        {0, 4, 4, 4, 1, 4, 4, 1, 4, 4, 4, 0},
        {0, 0, 4, 4, 1, 4, 4, 4, 0, 0, 0, 0},
        {0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0},
        {0, 0, 0, 2, 2, 2, 2, 5, 5, 5, 5, 0},
        {0, 0, 2, 0, 0, 2, 2, 2, 5, 2, 2, 2},
        {0, 0, 2, 0, 2, 2, 2, 5, 2, 2, 2, 0},
        {0, 0, 0, 0, 0, 2, 2, 5, 2, 0, 0, 0},
        {0, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 0},
        {0, 0, 0, 4, 4, 4, 4, 4, 4, 0, 0, 0},
    };

    scene_data result = {0};
    result.use_sky_background = true;
    result.cam.position = V3(224.0f, 262.0f, 605.0f);
    result.cam.rotation = V3(-14.0f, 14.0f, 0.0f);
    result.cam.fov = 40.0f;
    result.cam.aperture = 0.0f;
    result.cam.focusDist = 1.0f;
    result.cam.speed = defaultCameraSpeed;

    // Rects
    size_t boxCount = 20 * 20 * 2;
    size_t rectCount = 2 + (boxCount * 6);
    size_t rectAlloc = sizeof(rect_data) * rectCount;
    result.rects.data = malloc(rectAlloc);
    memset(result.rects.data, 0, rectAlloc);
    rect_list *rects = &result.rects;

    i32 width = 12;
    i32 height = 16;
    f32 pixelSize = 10.0f;
    for(i32 y = 0; y < height; ++y)
    {
        for(i32 x = 0; x < width; ++x)
        {
            if(isVisible[y][x])
            {
                f32 xMinPos = pixelSize*(f32)x;
                f32 xMaxPos = xMinPos + pixelSize;
                f32 yMinPos = pixelSize*(f32)y;
                f32 yMaxPos = yMinPos + pixelSize;
                rects->data[rects->count].v0 = V4(xMinPos, yMinPos, 0.0f, 0.0f);
                rects->data[rects->count].v1 = V4(xMaxPos, yMinPos, 0.0f, 0.0f);
                rects->data[rects->count].v2 = V4(xMinPos, yMaxPos, 0.0f, 0.0f);
                rects->data[rects->count].material_type = M_LAMBERTIAN;
                rects->data[rects->count].base_color = colors[colorMap[y][x]];
                rects->data[rects->count].emit = v4_to_v3(rects->data[rects->count].base_color);
                rects->data[rects->count].ref_index = 1.5f;
                rects->data[rects->count].id = (i32)rects->count;
                rects->count++;
            }

        }
    }

    return result;
}

internal scene_data CreateMirrorScene(u32 *rng)
{
    scene_data result = {0};
    result.use_sky_background = false;
    result.cam.position = V3(92.0f, 184.0f, 491.0f);
    result.cam.rotation = V3(-7.0f, 13.0f, 0.0f);
    result.cam.fov = 40.0f;
    result.cam.aperture = 0.0f;
    result.cam.focusDist = 1.0f;
    result.cam.speed = defaultCameraSpeed;

    size_t rectCount = 4;
    size_t rectAlloc = sizeof(rect_data) * rectCount;
    result.rects.data = malloc(rectAlloc);
    memset(result.rects.data, 0, rectAlloc);
    rect_list *rects = &result.rects;

    rects->data[rects->count].v0 = V4(-100.0f, 10.0f, 100.0f, 0.0f);
    rects->data[rects->count].v1 = V4(100.0f, 10.0f, 100.0f, 0.0f);
    rects->data[rects->count].v2 = V4(-100.0f, 10.0f, -100.0f, 0.0f);
    rects->data[rects->count].material_type = M_LAMBERTIAN;
    rects->data[rects->count].base_color = V4(0.75f, 0.75f, 0.75f, 0.0f);
    rects->data[rects->count].emit = V3(0.0f, 0.0f, 0.0f);
    rects->data[rects->count].id = (i32)rects->count;
    rects->count++;

    rects->data[rects->count].v0 = V4(-110.0f, 0.0f, -50.0f, 0.0f);
    rects->data[rects->count].v1 = V4(110.0f, 0.0f, -50.0f, 0.0f);
    rects->data[rects->count].v2 = V4(-110.0f, 250.0f, -50.0f, 0.0f);
    rects->data[rects->count].material_type = M_METAL;
    rects->data[rects->count].base_color = V4_1(0.5f);
    rects->data[rects->count].emit = V3(0.0f, 0.0f, 0.0f);
    rects->data[rects->count].id = (i32)rects->count;
    rects->count++;

    rects->data[rects->count].v0 = V4(110.0f, 0.0f, 50.0f, 0.0f);
    rects->data[rects->count].v1 = V4(-110.0f, 0.0f, 50.0f, 0.0f);
    rects->data[rects->count].v2 = V4(110.0f, 250.0f, 50.0f, 0.0f);
    rects->data[rects->count].material_type = M_METAL;
    rects->data[rects->count].base_color = V4_1(0.5f);
    rects->data[rects->count].emit = V3(0.0f, 0.0f, 0.0f);
    rects->data[rects->count].id = (i32)rects->count;
    rects->count++;


    rects->data[rects->count].v0 = V4(100.0f, 250.0f, 100.0f, 0.0f);
    rects->data[rects->count].v1 = V4(-100.0f, 250.0f, 100.0f, 0.0f);
    rects->data[rects->count].v2 = V4(100.0f, 250.0f, -100.0f, 0.0f);
    rects->data[rects->count].material_type = M_LAMBERTIAN;
    rects->data[rects->count].base_color = V4(0.75f, 0.75f, 0.75f, 0.0f);
    rects->data[rects->count].emit = V3(1.0f, 0.0f, 0.0f);
    rects->data[rects->count].id = (i32)rects->count;
    rects->count++;


    size_t sphereCount = 16;
    size_t sphereAlloc = sizeof(sphere) * sphereCount;
    result.spheres.data = malloc(sphereAlloc);
    memset(result.spheres.data, 0, sphereAlloc);
    sphere_list *spheres = &result.spheres;

    f32 interval = (2.0f*PI / 14);
    for(size_t i = 0; i < 14; ++i)
    {
        spheres->data[spheres->count].pos = V4(Cos((f32)i*interval) * 100.0f, Sin((f32)i*interval) * 100.0f + 130.0f, 0.0f, 0.0f);
        spheres->data[spheres->count].start_pos = spheres->data[spheres->count].pos;
        spheres->data[spheres->count].end_pos = spheres->data[spheres->count].pos;
        spheres->data[spheres->count].radius = 10.0f;
        spheres->data[spheres->count].material_type = M_LAMBERTIAN;

        float randomRed = 0.5f * (1.0f + GetRandom01(rng));
        float randomGreen = 0.5f * (1.0f + GetRandom01(rng));
        float randomBlue = 0.5f * (1.0f + GetRandom01(rng));
        spheres->data[spheres->count].base_color = V4(randomRed, randomGreen, randomBlue, 0.0f);
        //spheres->data[spheres->count].emit = V4(4.0f, 4.0f, 4.0f, 0.0f);
        spheres->data[spheres->count].id = (i32)spheres->count;
        spheres->count++;
    }

    spheres->data[spheres->count].pos = V4(0.0f, (260.0f / 2.0f), 0.0f, 0.0f);
    spheres->data[spheres->count].start_pos = spheres->data[spheres->count].pos;
    spheres->data[spheres->count].end_pos = spheres->data[spheres->count].pos;
    spheres->data[spheres->count].radius = 20.0f;
    spheres->data[spheres->count].material_type = M_LAMBERTIAN;
    spheres->data[spheres->count].base_color = V4(1.0f, 0.0f, 0.0f, 0.0f);
    spheres->data[spheres->count].emit = V4_1(2.5f);
    spheres->data[spheres->count].emit = V4(0.2f, 0.5f, 4.0f, 0.0f);
    spheres->data[spheres->count].id = (i32)spheres->count;
    spheres->count++;

    return result;
}


internal scene_data CreateCornellBoxScene(void)
{
    sphere_list spheres = {.data = NULL, .count = 0};

    size_t rectAllocSize = sizeof(rect_data) * (6 + 6 + 6);
    rect_data *rectList = malloc(rectAllocSize);
    memset(rectList, 0, rectAllocSize);
    i32 index = 0;

    v3 testBoxPos = {130.0f, 0.1f, 223.0f};
    AddAABox(rectList, testBoxPos, 150.0f, 250.0f, 150.0f, false, 0, M_LAMBERTIAN, (v3){0.0f, 0.0f, 0.0f}, false);
    RotateAABox(rectList, (v3){0.0f, 25.0f, 0.0f});
    index += 6;
    testBoxPos = (v3){310.0f, 0.1f, 400.0f};
    AddAABox(rectList + index, testBoxPos, 150.0f, 150.0f, 150.0f, false, index, M_LAMBERTIAN, (v3){0.0f, 0.0f, 0.0f}, false);
    RotateAABox(rectList + index, (v3){0.0f, -20.0f, 0.0f});
    index += 6;

#if 1
    // left
    rectList[index].v0 = V4(0.0f, 0.0f, 555.0f, 0.0f);
    rectList[index].v1 = V4(0.0f, 0.0f, 0.0f, 0.0f);
    rectList[index].v2 = V4(0.0f, 555.0f, 555.0f, 0.0f);
    rectList[index].radius = 1.0f;
    rectList[index].material_type = M_LAMBERTIAN;
    rectList[index].base_color = V4(0.12f, 0.45f, 0.15f, 0.0f);
    rectList[index].emit = V3(0.0f, 0.0f, 0.0f);
    rectList[index].id = index;
    ++index;

    // right
    rectList[index].v0 = V4(555.0f, 0.0f, 0.0f, 0.0f);
    rectList[index].v1 = V4(555.0f, 0.0f, 555.0f, 0.0f);
    rectList[index].v2 = V4(555.0f, 555.0f, 0.0f, 0.0f);
    rectList[index].radius = 1.0f;
    rectList[index].material_type = M_LAMBERTIAN;
    rectList[index].base_color = V4(0.65f, 0.05f, 0.05f, 0.0f);
    rectList[index].emit = V3(0.0f, 0.0f, 0.0f);
    rectList[index].id = index;
    ++index;

    // back
    rectList[index].v0 = V4(0.0f, 0.0f, 0.0f, 0.0f);
    rectList[index].v1 = V4(555.0f, 0.0f, 0.0f, 0.0f);
    rectList[index].v2 = V4(0.0f, 555.0f, 0.0f, 0.0f);
    rectList[index].radius = 1.0f;
    rectList[index].material_type = M_LAMBERTIAN;
    rectList[index].base_color = V4(0.73f, 0.73f, 0.73f, 0.0f);
    rectList[index].emit = V3(0.0f, 0.0f, 0.0f);
    rectList[index].id = index;
    ++index;

    // top
    rectList[index].v0 = V4(0.0f, 555.0f, 0.0f, 0.0f);
    rectList[index].v1 = V4(555.0f, 555.0f, 0.0f, 0.0f);
    rectList[index].v2 = V4(0.0f, 555.0f, 555.0f, 0.0f);
    rectList[index].radius = 1.0f;
    rectList[index].material_type = M_LAMBERTIAN;
    rectList[index].base_color = V4(0.73f, 0.73f, 0.73f, 0.0f);
    rectList[index].emit = V3(0.0f, 0.0f, 0.0f);
    rectList[index].id = index;
    ++index;

    // bottom
    rectList[index].v0 = V4(0.0f, 0.0f, 555.0f, 0.0f);
    rectList[index].v1 = V4(555.0f, 0.0f, 555.0f, 0.0f);
    rectList[index].v2 = V4(0.0f, 0.0f, 0.0f, 0.0f);
    rectList[index].radius = 1.0f;
    rectList[index].material_type = M_LAMBERTIAN;
    rectList[index].base_color = V4(1.0f, 1.0f, 1.0f, 0.0f);
    rectList[index].emit = V3(0.0f, 0.0f, 0.0f);
    rectList[index].id = index;
    ++index;

    // light
    f32 lWidth = 150.0f;
    f32 lXPosMin = (555.0f / 2.0f) - (lWidth / 2.0f);
    f32 lXPosMax = lXPosMin + lWidth;
    f32 lHeight = 150.0f;
    f32 lYPosMin = (555.0f / 2.0f) - (lHeight / 2.0f);
    f32 lYPosMax = lYPosMin + lHeight;

    rectList[index].v0 = V4(lXPosMin, 554.0f, lYPosMin, 0.0f);
    rectList[index].v1 = V4(lXPosMax, 554.0f, lYPosMin, 0.0f);
    rectList[index].v2 = V4(lXPosMin, 554.0f, lYPosMax, 0.0f);
    rectList[index].radius = 1.0f;
    rectList[index].material_type = M_LAMBERTIAN;
    rectList[index].base_color = V4(1.0f, 1.0f, 1.0f, 0.0f);
    rectList[index].emit = V3(15.0f, 15.0f, 15.0f);
    rectList[index].id = index;
    ++index;
#endif

    rect_list rects = {.data = rectList, .count = (u32)index};
    scene_data result;
    result.rects = rects;
    result.spheres = spheres;
    result.use_sky_background = false;
    result.cam.position = V3(273.0f, 273.0f, 1295.0f);
    result.cam.rotation = V3(0.0f, 0.0f, 0.0f);
    result.cam.fov = 40.0f;
    result.cam.aperture = 0.0f;
    result.cam.focusDist = 1.0f;
    result.cam.speed = defaultCameraSpeed;

    return result;
}

internal scene_data CreateSphereLightScene(u32 smallSphereCount, u32 *rng)
{
    sphere *sphereList = malloc(sizeof(sphere) * 4 * smallSphereCount);
    memset(sphereList, 0, sizeof(sphere) * 4 * smallSphereCount);
    i32 sphereIndex = 0;

    sphereList[sphereIndex].pos = V4(0.0f, -1000.0f, 0.0f, 0.0f);
    sphereList[sphereIndex].start_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].end_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].radius = 1000.0f;
    sphereList[sphereIndex].material_type = M_LAMBERTIAN;
    sphereList[sphereIndex].base_color = V4(0.5f, 0.5f, 0.5f, 0.0f);
    sphereList[sphereIndex].id = sphereIndex;

    ++sphereIndex;


    sphereList[sphereIndex].pos = V4(0.0f, 1.0f, 0.0f, 0.0f);
    sphereList[sphereIndex].start_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].end_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].radius = 1.0f;
    sphereList[sphereIndex].material_type = M_DIELECTRIC;
    sphereList[sphereIndex].base_color = V4(1.0f, 1.0f, 1.0f, 0.0f);
    sphereList[sphereIndex].fuzz = 0.0f;
    sphereList[sphereIndex].ref_index = 1.5f;
    sphereList[sphereIndex].id = sphereIndex;
    ++sphereIndex;

    sphereList[sphereIndex].pos = V4(-4.0f, 1.0f, 0.0f, 0.0f);
    sphereList[sphereIndex].start_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].end_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].radius = 1.0f;
    sphereList[sphereIndex].material_type = M_LAMBERTIAN;
    sphereList[sphereIndex].base_color = V4(0.4f, 0.2f, 0.1f, 0.0f);
    sphereList[sphereIndex].fuzz = 0.0f;
    sphereList[sphereIndex].id = sphereIndex;
    sphereList[sphereIndex].emit = V4(1.0f, 1.0f, 1.0f, 1.0f);
    ++sphereIndex;

    sphereList[sphereIndex].pos = V4(4.0f, 1.0f, 0.0f, 0.0f);
    sphereList[sphereIndex].start_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].end_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].radius = 1.0f;
    sphereList[sphereIndex].material_type = M_METAL;
    sphereList[sphereIndex].base_color = V4(0.7f, 0.6f, 0.5f, 0.0f);
    sphereList[sphereIndex].fuzz = 0.0f;
    sphereList[sphereIndex].id = sphereIndex;
    ++sphereIndex;

    i32 range = 20;
    for(i32 i = 0; i < (i32)smallSphereCount; ++i)
    {
        i32 a = (i / range) - (range / 2);
        i32 b = (i % range) - (range / 2) - 1;

        sphere *s = sphereList + sphereIndex;
        s->start_pos = v4_zero;
        s->end_pos = v4_zero;
        v3 center = V3(0.0f, 0.0f, 0.0f);
        bool validPos = false;
        while(!validPos)
        {
            validPos = true;
            center = V3(((f32)a)+0.9f*GetRandom01(rng), 0.22f, ((f32)b)+9.0f*GetRandom01(rng));
            for(i32 sIndex = 0; sIndex < sphereIndex; ++sIndex)
            {
                sphere *s2 = sphereList + sIndex;
                f32 radii = s2->radius + 0.2f;
                v3 pos2 = v4_to_v3(s2->pos);
                f32 l = v3_Length(v3_Sub(center, pos2));
                if(l < radii)
                {
                    validPos = false;
                    break;
                }
            }
        }

        float chooseMat = GetRandom01(rng);
        if(v3_Length(v3_Sub(center, V3(4.0f, 0.0f, 0.0f))) > 0.9f)
        {
            s->start_pos = v3_to_v4(center);
            s->end_pos = v3_to_v4(center);
            if(chooseMat < 0.8f)
            {
                s->pos = V4(center.x, center.y, center.z, 0.0f);
                s->end_pos = v3_to_v4(v3_Add(center, V3(0.0f, GetRandom01(rng) * 0.05f, 0.0f)));
                s->radius = 0.2f;
                s->material_type = M_LAMBERTIAN;
                float randomRed = 0.5f * (1.0f + GetRandom01(rng));
                float randomGreen = 0.5f * (1.0f + GetRandom01(rng));
                float randomBlue = 0.5f * (1.0f + GetRandom01(rng));
                s->base_color = V4(randomRed, randomGreen, randomBlue, 0.0f);
                s->fuzz = 0.0f;
            }
            else if(chooseMat < 0.95f)
            {

                s->pos = V4(center.x, center.y, center.z, 0.0f);
                s->radius = 0.2f;
                s->material_type = M_METAL;
                float randomRed = 0.5f * (1.0f + GetRandom01(rng));
                float randomGreen = 0.5f * (1.0f + GetRandom01(rng));
                float randomBlue = 0.5f * (1.0f + GetRandom01(rng));
                s->base_color = V4(randomRed, randomGreen, randomBlue, 0.0f);
                s->fuzz = 0.0f;
            }
            else
            {
                s->pos = V4(center.x, center.y, center.z, 0.0f);
                s->radius = 0.2f;
                s->material_type = M_DIELECTRIC;
                s->base_color = V4(1.0f, 1.0f, 1.0f, 0.0f);
                s->ref_index = 1.5f;
            }
        }
        else
        {
            // TODO(torgrim): These shouldn't really be added
            // to the sphere set at all...
            s->pos = V4(0.0f, -100.0f, 0.0f, 0.0f);
            s->radius = 0.2f;
            s->material_type = M_METAL;
            s->base_color = V4(1.0f, 1.0f, 1.0f, 0.0f);
            s->ref_index = 1.5f;
        }

        s->id = sphereIndex;
        ++sphereIndex;
    }

    sphere_list resultList = {.data = sphereList, .count = (u32)sphereIndex};
    tbs_assert(resultList.count == smallSphereCount + 4);

    scene_data result;
    result.spheres = resultList;
    result.rects = (rect_list){0};
    result.use_sky_background = false;
    result.cam.position = V3(2.0f, 3.0f, 4.6f);
    result.cam.rotation = V3(-16.0f, 57.0f, 0.0f);
    result.cam.fov = 40.0f;
    result.cam.aperture = 0.0f;
    result.cam.focusDist = 1.0f;
    result.cam.speed = defaultCameraSpeed;

    return result;
}

// TODO(torgrim): Spheres should also be able to use textures as their base color.
internal scene_data CreateDefaultScene(u32 smallSphereCount, u32 *rng)
{
    sphere *sphereList = malloc(sizeof(sphere) * 4 * smallSphereCount);
    memset(sphereList, 0, sizeof(sphere) * 4 * smallSphereCount);
    i32 sphereIndex = 0;

    sphereList[sphereIndex].pos = V4(0.0f, -1000.0f, 0.0f, 0.0f);
    sphereList[sphereIndex].start_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].end_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].radius = 1000.0f;
    sphereList[sphereIndex].material_type = M_LAMBERTIAN;
    sphereList[sphereIndex].base_color = V4(0.5f, 0.5f, 0.5f, 0.0f);
    sphereList[sphereIndex].id = sphereIndex;

    ++sphereIndex;

    sphereList[sphereIndex].pos = V4(0.0f, 1.0f, 0.0f, 0.0f);
    sphereList[sphereIndex].start_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].end_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].radius = 1.0f;
    sphereList[sphereIndex].material_type = M_DIELECTRIC;
    sphereList[sphereIndex].base_color = V4(1.0f, 1.0f, 1.0f, 0.0f);
    sphereList[sphereIndex].fuzz = 0.0f;
    sphereList[sphereIndex].ref_index = 1.5f;
    sphereList[sphereIndex].id = sphereIndex;
    ++sphereIndex;

    sphereList[sphereIndex].pos = V4(-4.0f, 1.0f, 0.0f, 0.0f);
    sphereList[sphereIndex].start_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].end_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].radius = 1.0f;
    sphereList[sphereIndex].material_type = M_LAMBERTIAN;
    sphereList[sphereIndex].base_color = V4(0.4f, 0.2f, 0.1f, 0.0f);
    sphereList[sphereIndex].fuzz = 0.0f;
    sphereList[sphereIndex].id = sphereIndex;
    ++sphereIndex;

    sphereList[sphereIndex].pos = V4(4.0f, 1.0f, 0.0f, 0.0f);
    sphereList[sphereIndex].start_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].end_pos = sphereList[sphereIndex].pos;
    sphereList[sphereIndex].radius = 1.0f;
    sphereList[sphereIndex].material_type = M_METAL;
    sphereList[sphereIndex].base_color = V4(0.7f, 0.6f, 0.5f, 0.0f);
    sphereList[sphereIndex].fuzz = 0.0f;
    sphereList[sphereIndex].id = sphereIndex;
    ++sphereIndex;

    i32 range = 20;
    for(i32 i = 0; i < (i32)smallSphereCount; ++i)
    {
        i32 a = (i / range) - (range / 2);
        i32 b = (i % range) - (range / 2) - 1;

        sphere *s = sphereList + sphereIndex;
        s->start_pos = v4_zero;
        s->end_pos = v4_zero;
        v3 center = V3(0.0f, 0.0f, 0.0f);
        bool validPos = false;
        while(!validPos)
        {
            validPos = true;
            center = V3(((f32)a)+0.9f*GetRandom01(rng), 0.22f, ((f32)b)+9.0f*GetRandom01(rng));
            for(i32 sIndex = 0; sIndex < sphereIndex; ++sIndex)
            {
                sphere *s2 = sphereList + sIndex;
                f32 radii = s2->radius + 0.2f;
                v3 pos2 = v4_to_v3(s2->pos);
                f32 l = v3_Length(v3_Sub(center, pos2));
                if(l < radii)
                {
                    validPos = false;
                    break;
                }
            }
        }

        float chooseMat = GetRandom01(rng);
        if(v3_Length(v3_Sub(center, V3(4.0f, 0.0f, 0.0f))) > 0.9f)
        {
            s->start_pos = v3_to_v4(center);
            s->end_pos = v3_to_v4(center);
            if(chooseMat < 0.8f)
            {
                s->pos = V4(center.x, center.y, center.z, 0.0f);
                s->end_pos = v3_to_v4(v3_Add(center, V3(0.0f, GetRandom01(rng) * 0.2f, 0.0f)));
                s->radius = 0.2f;
                s->material_type = M_LAMBERTIAN;

                float randomRed = 0.5f * (1.0f + GetRandom01(rng));
                float randomGreen = 0.5f * (1.0f + GetRandom01(rng));
                float randomBlue = 0.5f * (1.0f + GetRandom01(rng));
                s->base_color = V4(randomRed, randomGreen, randomBlue, 0.0f);
                s->fuzz = 0.0f;
            }
            else if(chooseMat < 0.95f)
            {

                s->pos = V4(center.x, center.y, center.z, 0.0f);
                s->radius = 0.2f;
                s->material_type = M_METAL;
                float randomRed = 0.5f * (1.0f + GetRandom01(rng));
                float randomGreen = 0.5f * (1.0f + GetRandom01(rng));
                float randomBlue = 0.5f * (1.0f + GetRandom01(rng));
                float randomFuzz = GetRandom01(rng);
                s->base_color = V4(randomRed, randomGreen, randomBlue, 0.0f);
                s->fuzz = randomFuzz;
                s->fuzz = 0.0f;
            }
            else
            {
                s->pos = V4(center.x, center.y, center.z, 0.0f);
                s->radius = 0.2f;
                s->material_type = M_DIELECTRIC;
                s->base_color = V4(1.0f, 1.0f, 1.0f, 0.0f);
                s->ref_index = 1.5f;
            }
        }
        else
        {
            // TODO(torgrim): These shouldn't really be added
            // to the sphere set at all...
            s->pos = V4(0.0f, -100.0f, 0.0f, 0.0f);
            s->radius = 0.2f;
            s->material_type = M_METAL;
            s->base_color = V4(1.0f, 1.0f, 1.0f, 0.0f);
            s->ref_index = 1.5f;
        }

        s->id = sphereIndex;
        ++sphereIndex;
    }

    sphere_list resultList = {.data = sphereList, .count = (u32)sphereIndex};
    tbs_assert(resultList.count == smallSphereCount + 4);

    scene_data result;
    result.spheres = resultList;
    result.rects = (rect_list){0};
    result.use_sky_background = true;
    result.cam.position = V3(13.0f, 5.0f, 10.5f);
    result.cam.rotation = V3(-14.0f, 52.0f, 0.0f);
    result.cam.fov = 40.0f;
    result.cam.aperture = 0.0f;
    result.cam.focusDist = 1.0f;
    result.cam.speed = defaultCameraSpeed;

    return result;
}

