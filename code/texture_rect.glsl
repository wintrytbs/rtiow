//@tbs_vert

#version 450 core

layout(location = 0) in vec2 in_vert_pos;
layout(location = 1) in vec2 in_tex_coord;

out vec2 tex_coords;

void main()
{
    tex_coords = in_tex_coord;
    gl_Position = vec4(in_vert_pos, 0.0f, 1.0f);
}


//@tbs_frag

#version 450 core

in vec2 tex_coords;

out vec4 frag_color;

uniform sampler2D tex;

void main()
{
    frag_color = texture(tex, tex_coords);
}
