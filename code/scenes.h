#ifndef SCENES_H
typedef enum scene_ids
{
    Scene_Default,
    Scene_SphereLight,
    Scene_CornellBox,
    Scene_Mirror,
    Scene_PixelMario,
    Scene_Stairs,
    Scene_Test,

    Scene_Count,
} scene_ids;

typedef struct scene_data
{
    simple_camera cam;
    bool use_sky_background;
    sphere_list spheres;
    rect_list rects;
} scene_data;

#define SCENES_H
#endif
