#if defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weverything"
#elif defined(_MSC_VER)
#pragma warning(push, 0)
#else
#error Unsupported Compiler
#endif
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>

#include <gl/GL.h>

#include <opengl/glext.h>
#include <opengl/wglext.h>

#if defined(__clang__)
#pragma GCC diagnostic pop
#elif defined(_MSC_VER)
#pragma warning(pop)
#else
#error Unsupported Compiler
#endif


#include "tbs_types.h"
#include "tbs_math.h"
#include "win32_rtiow.h"

#define AllocArray(type, count) malloc(sizeof(type) * count)

#include "gl_function_loader.c"
#include "win32_opengl.c"
#include "geometry.c"
#include "scenes.c"

// TODO(torgrim): Probably want to use an enum or ints here instead
// so that we can support as many keys as we want.
#define KeyMap_Escape (1ull << 0)
#define KeyMap_1 (1ull << 1)
#define KeyMap_2 (1ull << 2)
#define KeyMap_3 (1ull << 3)
#define KeyMap_4 (1ull << 4)
#define KeyMap_5 (1ull << 5)
#define KeyMap_6 (1ull << 6)
#define KeyMap_7 (1ull << 7)
#define KeyMap_8 (1ull << 8)
#define KeyMap_9 (1ull << 9)
#define KeyMap_0 (1ull << 10)
#define KeyMap_A (1ull << 11)
#define KeyMap_B (1ull << 12)
#define KeyMap_D (1ull << 13)
#define KeyMap_E (1ull << 14)
#define KeyMap_I (1ull << 15)
#define KeyMap_N (1ull << 16)
#define KeyMap_O (1ull << 17)
#define KeyMap_Q (1ull << 18)
#define KeyMap_R (1ull << 19)
#define KeyMap_S (1ull << 20)
#define KeyMap_W (1ull << 21)
#define KeyMap_Plus (1ull << 22)
#define KeyMap_Minus (1ull << 23)

#define IMAGE_SIZE_X 1080
#define IMAGE_SIZE_Y 1080

#define CAMERA_MAX_VELOCITY 1.0f

// TODO(torgrim): Consider putting this into some sort of state structure
// that we can pass around instead of having this in global scope.
global bool globalRunning;
global bool restartRender = false;
global i32 maxBounceCount = 50;
global i32 maxSampleCount = 10000;
global const f32 camRotationSpeed = 20.0f;

// NOTE(torgrim): From Nathan Reed http://www.reedbeta.com/blog/quick-and-easy-gpu-random-numbers-in-d3d11/
internal u32 WangHash(u32 seed)
{
    seed = (seed ^ 61u) ^ (seed >> 16u);
    seed *= 9u;
    seed = seed ^ (seed >> 4);
    seed *= 0x27d4eb2d;
    seed = seed ^ (seed >> 15);
    return seed;
}

internal f32 GetRandom01(u32 *state)
{
    *state = WangHash(*state);
    return ((f32)*state) * (1.0f / 4294967296.0f);
}

internal u32 CreateRngState(u32 a, u32 b)
{
    u32 rngState = 1664525u * a + 1013904223u * + b;
    //uint rngState = (a * 1973u + b + 9277u + 26699u) | 1u;
    return rngState;
}

internal char *tbs_CopyString(const char *s)
{
    char *result = NULL;
    size_t l = strlen(s);
    if(l > 0)
    {
        result = malloc(l + 1);
        memcpy(result, s, l);
        result[l] = '\0';
    }

    return result;
}

// NOTE(torgrim): Excluding the new line character.
internal line_info GetNextLine(u8 *cur)
{
    line_info result = {.start = cur};
    while(*cur != '\n' && *cur != '\r' && *cur != '\0')
    {
        ++cur;
        ++result.length;
    }

    if(result.length == 0)
    {
        result.start = 0;
    }
    return result;
}

internal inline bool LineEqual(line_info line, u8 *match, size_t matchLength)
{
    if(line.length != matchLength)
    {
        return false;
    }

    bool result = true;
    for(size_t i = 0; i < line.length; ++i)
    {
        if(line.start[i] != match[i])
        {
            result = false;
            break;
        }
    }

    return result;
}

internal inline LARGE_INTEGER Win32GetWallclockTime(void)
{
    LARGE_INTEGER result;
    QueryPerformanceCounter(&result);

    return result;
}

internal f32 Win32GetElapsedSeconds(LARGE_INTEGER startTime, LARGE_INTEGER endTime)
{
    LONGLONG elapsedTicks = endTime.QuadPart - startTime.QuadPart;

    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);
    LONGLONG elapsedMicroseconds = elapsedTicks * 1000000;
    elapsedMicroseconds /= freq.QuadPart;

    f32 seconds = (f32)elapsedMicroseconds / 1000000.0f;

    return seconds;
}

internal FILETIME Win32GetLastWriteTime(const char *filename)
{
    WIN32_FILE_ATTRIBUTE_DATA fileAtt = {0};
    // TODO: error handling
    GetFileAttributesEx(filename, GetFileExInfoStandard, &fileAtt);

    FILETIME result = fileAtt.ftLastWriteTime;
    return result;
}


internal file_data Win32ReadFile(const char *fileName, bool zeroTerminate)
{
    file_data result = {0};
    HANDLE fileHandle = CreateFile(fileName,
                                   GENERIC_READ,
                                   FILE_SHARE_READ,
                                   NULL,
                                   OPEN_EXISTING,
                                   0,
                                   0);

    if(fileHandle == INVALID_HANDLE_VALUE)
    {
        DWORD errorCode = GetLastError();

        // NOTE: access denied. This is most often caused by when we hot reload
        // shader files because the editor where the edit was made is holding
        // a lock on the file for some time. If this is the cause we try sleep and
        // try again.
        if(errorCode == 32)
        {
            Sleep(1000);

            fileHandle = CreateFile(fileName,
                                    GENERIC_READ,
                                    FILE_SHARE_READ,
                                    NULL,
                                    OPEN_EXISTING,
                                    0,
                                    0);
        }

        if(fileHandle == INVALID_HANDLE_VALUE)
        {
            tbs_assert(false);
            PrintDebugText("Invalid file handle\n");
            char errBuffer[255];
            sprintf_s(errBuffer, 255, "Error Code: %ld\n", GetLastError());

            PrintDebugText(errBuffer);

            return result;
        }
    }

    DWORD fileSize = GetFileSize(fileHandle, NULL);
    size_t allocSize = fileSize;
    if(zeroTerminate)
    {
        allocSize += 1;
    }
    u8 *file_content = (u8 *)VirtualAlloc(NULL, allocSize * sizeof(i8), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    DWORD bytesRead;
    BOOL readSuccess = ReadFile(fileHandle, (void *)file_content, fileSize, &bytesRead, NULL);
    if(readSuccess == FALSE)
    {
        tbs_assert(false);
        PrintDebugText("Could not read file\n");
    }
    else if(zeroTerminate)
    {
        file_content[bytesRead] = '\0';
        result.content = file_content;
        result.length = bytesRead + 1;
    }
    else
    {
        result.content = file_content;
        result.length = bytesRead;
    }

    CloseHandle(fileHandle);

    return result;
}

internal file_data Win32ReadAllText(const char *filename)
{
    file_data result = Win32ReadFile(filename, true);
    return result;
}

internal bool Win32FreeFileInfo(file_data *fileInfo)
{
    if(VirtualFree(fileInfo->content, 0, MEM_RELEASE) == FALSE)
    {
        tbs_assert(false);
        PrintDebugText("ERROR::WIN32:: could not free memory\n");
        return false;
    }
    else
    {
        fileInfo->content = NULL;
        fileInfo->length = 0;
        return true;
    }
}

internal void HandleKeyboardInput(u64 keyCode, bool isDown, bool wasDown, input_state *inputState)
{
    // TODO(torgrim): Handle cases where multiple up/down messages are processed within the same
    // frame so that we don't end up skipping any key presses.
    switch(keyCode)
    {
        case VK_ESCAPE:
        {
            inputState->keyboard.frameStates ^= KeyMap_Escape;
        }break;
        case VK_ADD:
        {
            inputState->keyboard.frameStates ^= KeyMap_Plus;
        }break;
        case VK_SUBTRACT:
        {
            inputState->keyboard.frameStates ^= KeyMap_Minus;
        }break;
        case '1':
        {
            inputState->keyboard.frameStates ^= KeyMap_1;
        }break;
        case '2':
        {
            inputState->keyboard.frameStates ^= KeyMap_2;
        }break;
        case '3':
        {
            inputState->keyboard.frameStates ^= KeyMap_3;
        }break;
        case '4':
        {
            inputState->keyboard.frameStates ^= KeyMap_4;
        }break;
        case '5':
        {
            inputState->keyboard.frameStates ^= KeyMap_5;
        }break;
        case '6':
        {
            inputState->keyboard.frameStates ^= KeyMap_6;
        }break;
        case '7':
        {
            inputState->keyboard.frameStates ^= KeyMap_7;
        }break;
        case 'A':
        {
            inputState->keyboard.frameStates ^= KeyMap_A;
        }break;
        case 'D':
        {
            inputState->keyboard.frameStates ^= KeyMap_D;
        }break;
        case 'S':
        {
            inputState->keyboard.frameStates ^= KeyMap_S;
        }break;
        case 'W':
        {
            inputState->keyboard.frameStates ^= KeyMap_W;
        }break;
        case 'Q':
        {
            inputState->keyboard.frameStates ^= KeyMap_Q;
        }break;
        case 'E':
        {
            inputState->keyboard.frameStates ^= KeyMap_E;
        }break;
        case 'R':
        {
            inputState->keyboard.frameStates ^= KeyMap_R;
        }break;
        case 'I':
        {
            inputState->keyboard.frameStates ^= KeyMap_I;
        }break;
        case 'O':
        {
            inputState->keyboard.frameStates ^= KeyMap_O;
        }break;
        case 'B':
        {
            inputState->keyboard.frameStates ^= KeyMap_B;
        } break;
        case 'N':
        {
            inputState->keyboard.frameStates ^= KeyMap_N;
        } break;
    }


}

internal void ProcessInput(application_context *ctx, input_state *inputState, simple_camera *cam, f32 dt)
{
    if(inputState->mouse.rightButtonDown)
    {
        matrix3D camRot = m3x3_CreateRotateV3(cam->rotation);
        v3 camAxisX = m3x3_GetColumnVector(camRot, 0);
        v3 camAxisZ = m3x3_GetColumnVector(camRot, 2);

        // TODO(torgrim): Make this a smooth motion.
        if(inputState->keyboard.prevStates & KeyMap_W)
        {
            cam->position = v3_Sub(cam->position, v3_Mulf32(camAxisZ, cam->speed * dt));
        }
        if(inputState->keyboard.prevStates & KeyMap_S)
        {
            cam->position = v3_Add(cam->position, v3_Mulf32(camAxisZ, cam->speed * dt));
        }
        if(inputState->keyboard.prevStates & KeyMap_A)
        {
            cam->position = v3_Sub(cam->position, v3_Mulf32(camAxisX, cam->speed * dt));
        }
        if(inputState->keyboard.prevStates & KeyMap_D)
        {
            cam->position = v3_Add(cam->position, v3_Mulf32(camAxisX, cam->speed * dt));
        }

        ctx->viewerMode = VIEWER_MODE_FlyCamera;
    }
    else
    {
        if(ctx->viewerMode != VIEWER_MODE_FullRender)
        {
            ctx->viewerMode = VIEWER_MODE_FullRender;
            restartRender = true;
        }
    }

    // TODO(torgrim): Since we get the same keystrokes over multiple frames
    // this kind of handling doesn't work very well when we want to adjust some
    // value discretely on keydown events.
    // Should probably handle just keyup instead then or?
    if(inputState->keyboard.buttonDowns & KeyMap_R)
    {
        restartRender = true;
        cam->focusDist = 1.0f;
        cam->aperture = 0.0f;
    }
    if(inputState->keyboard.buttonDowns & KeyMap_Plus)
    {
        PrintDebugText("Increasing target bounce count\n");
        maxBounceCount += 1;
        restartRender = true;
    }
    if(inputState->keyboard.buttonDowns & KeyMap_Minus)
    {
        if(maxBounceCount > 1)
        {
            maxBounceCount -= 1;
            restartRender = true;
        }
    }
    if(inputState->keyboard.buttonDowns & KeyMap_O)
    {
        ctx->useMotionBlur = !ctx->useMotionBlur;
        restartRender = true;
    }
}

internal void ComputeInitRays(application_context *ctx, compute_mode mode, shader_meta_info *shader, simple_camera *cam)
{
    tbs_assert(mode == COMPUTE_MODE_CameraRaysInit || mode == COMPUTE_MODE_SampleInit);

    f32 viewAspect = (f32)IMAGE_SIZE_X / (f32)IMAGE_SIZE_Y;
    f32 verticalHalf = Tanf32(DegreeToRadians(cam->fov * 0.5f));
    f32 horizontalHalf = viewAspect * verticalHalf;

    matrix4D view = m4x4_Mul(m4x4_CreateTranslateV3(cam->position), m4x4_CreateRotateV3(cam->rotation));

    glUniform2f(shader->uniforms[RTXShaderUniform_image_plane], horizontalHalf, verticalHalf);
    glUniform3f(shader->uniforms[RTXShaderUniform_cam_pos], cam->position.x, cam->position.y, cam->position.z);
    glUniform1f(shader->uniforms[RTXShaderUniform_cam_aperture], cam->aperture);
    glUniform1i(shader->uniforms[RTXShaderUniform_process_mode], mode);
    glUniformMatrix4fv(shader->uniforms[RTXShaderUniform_view], 1, GL_FALSE, &(view.n[0][0]));
    glUniform1i(shader->uniforms[RTXShaderUniform_use_motion_blur], ctx->useMotionBlur);

    glDispatchCompute(ctx->workGroupCountX, ctx->workGroupCountY, 1);

    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
}

internal void ComputeAndRender(application_context *ctx, shader_meta_info *shaders, scene_data *scene)
{
    shader_meta_info *rtxShader = shaders + ShaderID_RTX;
    shader_meta_info *texturedRectShader = shaders + ShaderID_TexturedRect;

    glUseProgram(rtxShader->programID);
    glUniform1i(rtxShader->uniforms[RTXShaderUniform_use_sky_background], scene->use_sky_background);
    glUniform1f(rtxShader->uniforms[RTXShaderUniform_iTime], ctx->runtime);

    if(ctx->viewerMode == VIEWER_MODE_FlyCamera)
    {
        ctx->currentSampleCount = 0;
        ComputeInitRays(ctx, COMPUTE_MODE_CameraRaysInit, rtxShader, &scene->cam);

        {
            glUniform1i(rtxShader->uniforms[RTXShaderUniform_process_mode], COMPUTE_MODE_Simple);

            glDispatchCompute(ctx->workGroupCountX, ctx->workGroupCountY, 1);
            glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
        }
    }
    else if(ctx->viewerMode == VIEWER_MODE_FullRender)
    {
        if(ctx->initMode != COMPUTE_MODE_None)
        {
            if(ctx->initMode == COMPUTE_MODE_CameraRaysInit)
            {
                ctx->currentSampleCount = 0;
            }

            ComputeInitRays(ctx, ctx->initMode, rtxShader, &scene->cam);

            {
                glUniform1i(rtxShader->uniforms[RTXShaderUniform_process_mode], COMPUTE_MODE_CollapsedCastScatter);

                for(i32 bCount = 0; bCount < maxBounceCount; ++bCount)
                {
                    glDispatchCompute(ctx->workGroupCountX, ctx->workGroupCountY, 1);
                    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
                }
            }

            ctx->initMode = COMPUTE_MODE_None;
        }
        else
        {
            glUniform1i(rtxShader->uniforms[RTXShaderUniform_process_mode], COMPUTE_MODE_SampleEnd);

            glDispatchCompute(ctx->workGroupCountX, ctx->workGroupCountY, 1);
            glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

            ++ctx->currentSampleCount;
            if(ctx->currentSampleCount == maxSampleCount)
            {
                PrintDebugText("Render Finished\n");
                ctx->viewerMode = VIEWER_MODE_RenderDone;
            }
            else
            {
                PrintDebugText("Current Sample %d\n", ctx->currentSampleCount);
                ctx->initMode = COMPUTE_MODE_SampleInit;
            }
        }
    }

    glUniform1i(rtxShader->uniforms[RTXShaderUniform_process_mode], COMPUTE_MODE_Render);
    glUniform1i(rtxShader->uniforms[RTXShaderUniform_current_sample_index], ctx->currentSampleCount);

    glDispatchCompute(ctx->workGroupCountX, ctx->workGroupCountY, 1);
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);


    //sprintf(outputBuffer, "Current Sample Count: %d\n", currentSampleCount);
    //OutputDebugString(outputBuffer);

    glViewport(0, 0, IMAGE_SIZE_X, IMAGE_SIZE_Y);
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(texturedRectShader->programID);
    glUniform1i(texturedRectShader->uniforms[TexturedRectShaderUniform_tex], 0);

    glBindVertexArray(ctx->targetVAOID);
    glDrawArrays(GL_TRIANGLES, 0, 6);

    glBindVertexArray(0);
    glUseProgram(0);
}

internal LRESULT CALLBACK MainWindowCallback(HWND window, UINT msg, WPARAM wParam, LPARAM lParam)
{
    LRESULT result = 0;

    switch(msg)
    {
        case WM_DESTROY:
        case WM_CLOSE:
        {
            globalRunning = false;
        }break;
        default:
        {
            result = DefWindowProcA(window, msg, wParam, lParam);
        }break;
    }

    return result;
}

int WinMain(HINSTANCE instance, HINSTANCE prevInstance, PSTR cmdLine, int cmdShow)
{
    u32 rngState = CreateRngState(17, 37);

    HWND windowHandle = Win32InitOpenGL(instance, MainWindowCallback, IMAGE_SIZE_X, IMAGE_SIZE_Y, 4, 5);
    if(windowHandle != INVALID_HANDLE_VALUE)
    {
        GLint maxComputeWGInvocation;
        GLint maxComputeWGCount[3];
        GLint maxComputeWGSize[3];
        glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &maxComputeWGInvocation);
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &maxComputeWGCount[0]);
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &maxComputeWGCount[1]);
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &maxComputeWGCount[2]);

        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &maxComputeWGSize[0]);
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &maxComputeWGSize[1]);
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &maxComputeWGSize[2]);

        GLint maxUniformBufferBinding;
        GLint maxUniformComputeComponents;
        glGetIntegerv(GL_MAX_UNIFORM_BUFFER_BINDINGS, &maxUniformBufferBinding);
        glGetIntegerv(GL_MAX_COMPUTE_UNIFORM_COMPONENTS, &maxUniformComputeComponents);

        shader_meta_info *shaders = OpenGLInitShaders();

        f32 vertexAttribs[] =
        {
            -1.0f, -1.0f, 0.0f, 0.0f,
             1.0f, -1.0f, 1.0f, 0.0f,
             1.0f,  1.0f, 1.0f, 1.0f,
             1.0f,  1.0f, 1.0f, 1.0f,
            -1.0f,  1.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, 0.0f, 0.0f,
        };

        GLuint mainVAOID;
        GLuint mainVBOID;
        glGenVertexArrays(1, &mainVAOID);
        glGenBuffers(1, &mainVBOID);

        glBindVertexArray(mainVAOID);
        glBindBuffer(GL_ARRAY_BUFFER, mainVBOID);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertexAttribs), vertexAttribs, GL_STATIC_DRAW);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(f32) * 4, 0);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(f32) * 4, (void *)(uintptr_t)(sizeof(f32) * 2));
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        size_t imageByteSize = sizeof(f32) * IMAGE_SIZE_X * IMAGE_SIZE_Y * 4;
        f32 *zeroTexture = malloc(imageByteSize);
        memset(zeroTexture, 0, imageByteSize);
        glActiveTexture(GL_TEXTURE0);
        GLuint rayTex;
        glGenTextures(1, &rayTex);
        glBindTexture(GL_TEXTURE_2D, rayTex);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, IMAGE_SIZE_X, IMAGE_SIZE_Y, 0, GL_RGBA, GL_FLOAT, zeroTexture);
        glBindImageTexture(0, rayTex, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);

        scene_data scenes[Scene_Count];
        scenes[Scene_Default] = CreateDefaultScene(512, &rngState);
        scenes[Scene_SphereLight] = CreateSphereLightScene(512, &rngState);
        scenes[Scene_CornellBox] = CreateCornellBoxScene();
        scenes[Scene_Mirror] = CreateMirrorScene(&rngState);
        scenes[Scene_PixelMario] = CreatePixelMarioScene();
        scenes[Scene_Stairs] = CreateStairsScene(&rngState);

        scene_data *activeScene = scenes + 0;
        GLuint sceneSphereData = OpenGLCreateShaderStorageBuffer(sizeof(sphere) * activeScene->spheres.count, GL_STATIC_DRAW, 2, activeScene->spheres.data);
        GLuint sceneRectData = OpenGLCreateShaderStorageBuffer(sizeof(rect_data) * activeScene->rects.count, GL_STATIC_DRAW, 4, activeScene->rects.data);

        size_t pixelInfoBufferSize = sizeof(pixel_info) * IMAGE_SIZE_X * IMAGE_SIZE_Y;
        OpenGLCreateShaderStorageBuffer(pixelInfoBufferSize, GL_DYNAMIC_READ, 3, NULL);

        f32 runtime = 0.0f;
        f32 dt = 1.0f / 60.0f;
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        globalRunning = true;

        input_state inputState = {0};
        POINT oldCursorPos = {0};
        POINT cursorOriginalPos = {0};

        LONG defaultWindowStyle = GetWindowLong(windowHandle, GWL_STYLE);

        application_context appCtx = {0};
        appCtx.targetVAOID = mainVAOID;
        appCtx.targetVBOID = mainVBOID;
        appCtx.initMode = COMPUTE_MODE_CameraRaysInit;
        appCtx.viewerMode = VIEWER_MODE_FullRender;
        appCtx.dt = dt;
        appCtx.runtime = runtime;

        while(globalRunning)
        {
            // TODO(torgrim): Reset all value in the keyboard inputmap in the inputState
            MSG msg;
            while(PeekMessageA(&msg, windowHandle, 0, 0, PM_REMOVE))
            {
                switch(msg.message)
                {
                    case WM_KEYUP:
                    case WM_KEYDOWN:
                    {
                        // TODO(torgrim): Add modifier keys here as well, like ctrl, shift and alt
                        // so that we can check if these were don't when the key was pressed. Use
                        // GetKeyState() for this.
                        bool wasDown = (((msg.lParam >> 30) & 1) != 0);
                        bool isDown = (((msg.lParam >> 31) & 1) == 0);
                        if(wasDown != isDown)
                        {
                            PrintDebugText("Button State Change\n");
                            PrintDebugText("Button wasDown = %d\n", wasDown);
                            PrintDebugText("Button isDown = %d\n", isDown);
                            u64 keyCode = msg.wParam;
                            HandleKeyboardInput(keyCode, isDown, wasDown, &inputState);
                        }
                    }break;
                    case WM_LBUTTONUP:
                    {
                        WORD mouseX = LOWORD(msg.lParam);
                        WORD mouseY = HIWORD(msg.lParam);
                        inputState.mouse.lastLeftUpPosClient = (v2){(f32)mouseX, (f32)mouseY};
                    }break;
                    case WM_RBUTTONDOWN:
                    {
                        SetCapture(windowHandle);
                        inputState.mouse.rightButtonDown = true;
                        RECT clientRect;
                        GetClientRect(windowHandle, &clientRect);
                        WORD mouseX = LOWORD(msg.lParam);
                        WORD mouseY = HIWORD(msg.lParam);
                        POINT clientP = {mouseX, mouseY};
                        ClientToScreen(windowHandle, &clientP);
                        oldCursorPos = clientP;
                        inputState.mouse.delta = v2_Zero;
                        ShowCursor(FALSE);
                        cursorOriginalPos = clientP;

                    }break;
                    case WM_RBUTTONUP:
                    {
                        inputState.mouse.rightButtonDown = false;
                        inputState.mouse.delta = v2_Zero;
                        ReleaseCapture();
                        ShowCursor(TRUE);
                        SetCursorPos(cursorOriginalPos.x, cursorOriginalPos.y);
                    }break;
                    case WM_MOUSEWHEEL:
                    {
                        SHORT dist = (SHORT)HIWORD(msg.wParam);
                        activeScene->cam.speed += (f32)((2 * dist) / 120) ;
                        if(activeScene->cam.speed < 0.0f)
                        {
                            activeScene->cam.speed = 0.0f;
                        }
                        PrintDebugText("Current Camera Speed: %f\n", (f64)activeScene->cam.speed);
                        PrintDebugText("MOUSE WHEEL DIST: %d\n", dist);
                    }break;
                    default:
                    {
                        TranslateMessage(&msg);
                        DispatchMessage(&msg);
                    }
                }
            }

            u64 buttonChanges = inputState.keyboard.frameStates ^ inputState.keyboard.prevStates;

            inputState.keyboard.buttonDowns = buttonChanges & inputState.keyboard.frameStates;
            inputState.keyboard.buttonUps = buttonChanges & (~inputState.keyboard.frameStates);

            inputState.keyboard.prevStates = inputState.keyboard.frameStates;

            LARGE_INTEGER frameStartTime = Win32GetWallclockTime();

            RECT clientRect;
            GetClientRect(windowHandle, &clientRect);

            CheckAndReloadModifiedShaders(shaders, ShaderID_Count);

            shader_meta_info *rtxShader = shaders + ShaderID_RTX;
            shader_meta_info *noiseShader = shaders + ShaderID_Noise;
            shader_meta_info *texturedRectShader = shaders + ShaderID_TexturedRect;

            if(rtxShader->uniformsLoaded == false)
            {
                OpenGLLoadUniforms(rtxShader, ShaderID_RTX);
            }

            if(noiseShader->uniformsLoaded == false)
            {
                OpenGLLoadUniforms(noiseShader, ShaderID_Noise);
            }

            if(texturedRectShader->uniformsLoaded == false)
            {
                OpenGLLoadUniforms(texturedRectShader, ShaderID_TexturedRect);
            }

            tbs_assert(rtxShader->uniformsLoaded);
            tbs_assert(noiseShader->uniformsLoaded);
            tbs_assert(texturedRectShader->uniformsLoaded);

            if(inputState.mouse.rightButtonDown)
            {
                POINT newCursorPos;
                GetCursorPos(&newCursorPos);
                inputState.mouse.delta.x = (f32)(newCursorPos.x - oldCursorPos.x);
                inputState.mouse.delta.y = (f32)-(newCursorPos.y - oldCursorPos.y);
                oldCursorPos = newCursorPos;

                activeScene->cam.rotation.y -= inputState.mouse.delta.x * dt * camRotationSpeed;
                activeScene->cam.rotation.x += inputState.mouse.delta.y * dt * camRotationSpeed;

                restartRender = true;
            }

            if(inputState.keyboard.buttonDowns & KeyMap_Escape)
            {
                globalRunning = false;
                continue;
            }

            ProcessInput(&appCtx, &inputState, &activeScene->cam, dt);

            if(inputState.keyboard.buttonDowns & KeyMap_B)
            {
                LONG noBorderStyle = defaultWindowStyle & (~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_SYSMENU));
                SetWindowLongPtrA(windowHandle, GWL_STYLE, noBorderStyle);
            }
            else if(inputState.keyboard.buttonDowns & KeyMap_N)
            {
                SetWindowLongPtrA(windowHandle, GWL_STYLE, defaultWindowStyle);
            }

            local_persist scene_ids currentSceneID = Scene_Default;
            scene_ids prevSceneID = currentSceneID;

            // TODO(torgrim): Maybe put this into process input?
            if(inputState.keyboard.buttonDowns & KeyMap_1)
            {
                if(currentSceneID != Scene_Default) { currentSceneID = Scene_Default; }
            }
            else if(inputState.keyboard.buttonDowns & KeyMap_2)
            {
                if(currentSceneID != Scene_SphereLight) { currentSceneID = Scene_SphereLight; }
            }
            else if(inputState.keyboard.buttonDowns & KeyMap_3)
            {
                if(currentSceneID != Scene_CornellBox) { currentSceneID = Scene_CornellBox; }
            }
            else if(inputState.keyboard.buttonDowns & KeyMap_4)
            {
                if(currentSceneID != Scene_Mirror) { currentSceneID = Scene_Mirror; }
            }
            else if(inputState.keyboard.buttonDowns & KeyMap_5)
            {
                if(currentSceneID != Scene_PixelMario) { currentSceneID = Scene_PixelMario; }
            }
            else if(inputState.keyboard.buttonDowns & KeyMap_6)
            {
                if(currentSceneID != Scene_Stairs) { currentSceneID = Scene_Stairs; }
            }
            else if(inputState.keyboard.buttonDowns & KeyMap_7)
            {
                //if(currentSceneID != Scene_Test) { currentSceneID = Scene_Test; }
            }

            if(prevSceneID != currentSceneID)
            {
                activeScene = scenes + currentSceneID;
                glBindBuffer(GL_SHADER_STORAGE_BUFFER, sceneSphereData);
                glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(sphere) * activeScene->spheres.count, activeScene->spheres.data, GL_STATIC_DRAW);
                glBindBuffer(GL_SHADER_STORAGE_BUFFER, sceneRectData);
                glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(rect_data) * activeScene->rects.count, activeScene->rects.data, GL_STATIC_DRAW);
                glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
                restartRender = true;
            }

            if(restartRender)
            {
                appCtx.initMode = COMPUTE_MODE_CameraRaysInit;
                restartRender = false;
            }

            if(appCtx.regenerateNoiseTexture)
            {
                glUseProgram(noiseShader->programID);
                glUniform1f(noiseShader->uniforms[NoiseShaderUniform_iTime], runtime);
                glDispatchCompute(IMAGE_SIZE_X, IMAGE_SIZE_Y, 1);

                glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
            }

            GLuint globalWorkGroupCountX = IMAGE_SIZE_X / 8;
            GLuint globalWorkGroupCountY = IMAGE_SIZE_Y / 8;

            appCtx.workGroupCountX = globalWorkGroupCountX;
            appCtx.workGroupCountY = globalWorkGroupCountY;
            appCtx.dt = dt;
            appCtx.runtime = runtime;

            ComputeAndRender(&appCtx, shaders, activeScene);

            HDC deviceContext = GetDC(windowHandle);
            SwapBuffers(deviceContext);

            OpenGLCatchError();

            LARGE_INTEGER frameEndTime = Win32GetWallclockTime();
            dt = Win32GetElapsedSeconds(frameStartTime, frameEndTime);
            runtime += dt;

            //PrintDebugText("Frame Time: %f\n", (f64)dt);
        }
    }

    return 0;
}
