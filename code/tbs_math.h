#ifndef TBS_MATH_H

#include "math.h"

#include "tbs_types.h"

global const f32 PI = 3.141592f;
global const f32 F32_EPSILON = 0.0001f;

// TODO(torgrim): try and organize this file so that
// you have type declarations separated from functions
// and still group functions belong to a specific type
//
// TODO(torgrim): define some safe ratio functions so that
// we don't have to worry about dividing by zero everywhere
// but just defaults to some given value

internal inline f32 Sin(f32 value)
{
    f32 result = sinf(value);
    return result;
}

internal inline f32 Cos(f32 value)
{
    f32 result = cosf(value);
    return result;
}

internal inline f32 Tanf32(f32 value)
{
    f32 result = tanf(value);
    return result;
}

internal inline f32 ArcSin(f32 value)
{
    f32 result = asinf(value);
    return result;
}

internal inline f32 ArcCos(f32 value)
{
    f32 result = acosf(value);
    return result;
}

internal inline f32 ArcTan2(f32 a, f32 b)
{
    f32 result = atan2f(a, b);
    return result;
}

internal inline f32 SquareRoot(f32 value)
{
    f32 result = sqrtf(value);
    return result;
}

internal inline f32 Pow2(f32 value)
{
    f32 result = value*value;
    return result;
}

internal inline f32 Absf32(f32 value)
{
    f32 result = fabsf(value);
    return result;
}

internal inline f32 RadianToDegree(f32 radian)
{
    f32 result = (180.0f * radian) / PI;

    return result;
}

internal inline f32 DegreeToRadians(f32 degrees)
{
    f32 result = (PI / 180.0f) * degrees;

    return result;
}


typedef struct v2
{
    f32 x;
    f32 y;
} v2;

internal inline v2 V2(f32 a, f32 b)
{
    v2 result = {a, b};
    return result;
}

internal inline v2 v2_Add(v2 a, v2 b)
{
    v2 result = {a.x + b.x, a.y + b.y};
    return result;
}

internal inline v2 v2_Sub(v2 a, v2 b)
{
    v2 result = {a.x - b.x, a.y - b.y};
    return result;
}

internal inline v2 v2_Subf32(v2 a, f32 s)
{
    v2 result = {a.x - s, a.y - s};
    return result;
}


internal inline v2 v2_Mul(v2 a, v2 b)
{
    v2 result = {a.x * b.x, a.y * b.y};
    return result;
}

internal inline v2 v2_Mulf32(v2 a, f32 scalar)
{
    v2 result = a;
    result.x *= scalar;
    result.y *= scalar;
    return result;
}

internal inline f32 v2_LengthSquared(v2 a)
{
    f32 result = (a.x*a.x) + (a.y*a.y);
    return result;
}

internal inline f32 v2_Length(v2 a)
{
    f32 l2 = v2_LengthSquared(a);
    f32 result = SquareRoot(l2);

    return result;
}

internal inline v2 v2_Normalize(v2 a)
{
    f32 l = v2_Length(a);

    v2 result;
    result.x = a.x / l;
    result.y = a.y / l;

    return result;
}

internal inline v2 v2_Lerp(v2 p0, v2 p1, f32 t)
{
    v2 s = v2_Mulf32(p0, 1.0f-t);
    v2 e = v2_Mulf32(p1, t);
    v2 result = v2_Add(s, e);
    return result;
}

internal inline bool v2_IsNearlyZero(v2 a)
{
    bool result = (Absf32(a.x) < F32_EPSILON && Absf32(a.y) < F32_EPSILON);
    return result;
}

// TODO(torgrim): implement this properly
typedef struct v3
{
    f32 x;
    f32 y;
    f32 z;
} v3;

internal inline v3 V3(f32 a, f32 b, f32 c)
{
    v3 result = {a, b, c};
    return result;
}

internal inline v3 V3_1(f32 a)
{
    v3 result = {a, a, a};
    return result;
}

internal inline v3 v3_Mul(v3 a, v3 b)
{
    v3 result = V3(a.x * b.x, a.y * b.y, a.z * b.z);
    return result;
}

internal inline v3 v3_Mulf32(v3 a, f32 s)
{
    v3 result = V3(a.x * s, a.y * s, a.z * s);
    return result;
}

internal inline v3 v3_Sub(v3 a, v3 b)
{
    v3 result = V3(a.x - b.x, a.y - b.y, a.z - b.z);
    return result;
}

internal inline v3 v3_Neg(v3 a)
{
    v3 result = V3(-a.x, -a.y, -a.z);
    return result;
}

internal inline v3 v3_Add(v3 a, v3 b)
{
    v3 result = V3(a.x + b.x, a.y + b.y, a.z + b.z);
    return result;
}

internal inline v3 v3_Addf32(v3 a, f32 r)
{
    v3 result = V3(a.x + r, a.y + r, a.z + r);
    return result;
}

internal inline v3 v3_Add3(v3 a, v3 b, v3 c)
{
    v3 result = v3_Add(v3_Add(a, b), c);
    return result;
}

internal inline v3 v3_Divf32(v3 a, f32 s)
{
    tbs_assert(s != 0.0f);
    v3 result = V3(a.x / s, a.y / s, a.z / s);
    return result;
}

internal inline f32 v3_LengthSquared(v3 a)
{
    f32 ls = (a.x * a.x) + (a.y * a.y) + (a.z * a.z);
    return ls;
}

internal inline f32 v3_Length(v3 a)
{
    f32 ls = v3_LengthSquared(a);
    f32 l = SquareRoot(ls);

    return l;
}

internal inline f32 v3_Dot(v3 a, v3 b)
{
    f32 result = (a.x * b.x + a.y * b.y + a.z * b.z);

    return result;
}

internal inline v3 v3_Cross(v3 a, v3 b)
{
    v3 result = V3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
    return result;
}

internal inline v3 v3_Normalize(v3 a)
{
    f32 l = v3_Length(a);
    tbs_assert(l != 0.0f);
    v3 result = v3_Divf32(a,l);
    return result;
}

internal inline v3 v3_Project(v3 a, v3 b)
{
    f32 l = v3_Length(b);
    tbs_assert(l != 0.0f);
    f32 r1 = v3_Dot(a,b) / l;
    v3 result = v3_Mulf32(b, r1);

    return result;
}

internal inline v3 v3_Reject(v3 a, v3 b)
{
    v3 result = v3_Sub(a, v3_Project(a, b));

    return result;
}

internal inline bool v3_Equal(v3 a, v3 b)
{
    bool result = (a.x == b.x && a.y == b.y && a.z == b.z);
    return result;
}

internal inline v3 v3_Max(v3 value, v3 max)
{
    v3 result = value;
    if(result.x > max.x)
    {
        result.x = max.x;
    }
    if(result.y > max.y)
    {
        result.y = max.y;
    }
    if(result.z > max.z)
    {
        result.z = max.z;
    }

    return result;
}

internal inline v3 v3_Maxf32(v3 value, f32 max)
{
    v3 result = value;
    if(result.x > max)
    {
        result.x = max;
    }
    if(result.y > max)
    {
        result.y = max;
    }
    if(result.z > max)
    {
        result.z = max;
    }

    return result;
}


// TODO(torgrim): make this for v2, v3, v4 and also
// one that defaults to min = 0, max = 1
internal inline f32 Clamp(f32 a, f32 min, f32 max)
{
    f32 result = a;
    if(a < min)
    {
        result = min;
    }
    else if(a > max)
    {
        result = max;
    }

    return result;
}

internal inline bool v3_IsZero(v3 a)
{
    bool result = (a.x == 0.0f && a.y == 0.0f && a.z == 0.0f);
    return result;
}

internal inline bool v3_IsNearlyZero(v3 a)
{
    bool result = (Absf32(a.x) < F32_EPSILON && 
                   Absf32(a.y) < F32_EPSILON && Absf32(a.z) < F32_EPSILON);
    return result;
}

// TODO(torgrim): implement v4 properly with helper functions
// and operators
typedef struct v4
{
    f32 x;
    f32 y;
    f32 z;
    f32 w;
} v4;

internal inline v4 V4(f32 a, f32 b, f32 c, f32 d)
{
    v4 result = {a, b, c, d};
    return result;
}

internal inline v4 V4_1(f32 a)
{
    v4 result = {a, a, a, a};
    return result;
}

#define v4_zero V4(0.0f, 0.0f, 0.0f, 0.0f);

internal inline v3 v4_to_v3(v4 a)
{
    v3 result = V3(a.x, a.y, a.z);
    return result;
}

internal inline v4 v3_to_v4(v3 a)
{
    v4 result = V4(a.x, a.y, a.z, 0.0f);
    return result;
}

internal inline v4 v4_Divf32(v4 a, f32 s)
{
    v4 result = V4(a.x / s, a.y / s, a.z / s, a.w / s);
    return result;
}

internal inline f32 v4_LengthSquared(v4 a)
{
    f32 ls = (a.x * a.x) + (a.y * a.y) + (a.z * a.z) + (a.w * a.w);
    return ls;
}

internal inline f32 v4_Length(v4 a)
{
    f32 ls = v4_LengthSquared(a);
    f32 l = SquareRoot(ls);

    return l;
}

internal inline v4 v4_Normalize(v4 a)
{
    f32 l = v4_Length(a);
    tbs_assert(l != 0.0f);
    v4 result = v4_Divf32(a,l);
    return result;
}

// TODO(torgrim): this isn't really the way
// to do this but works for now
internal inline bool v4_IsNormalized(v4 a)
{
    const f32 epsilon = 0.000001f;
    f32 l = v4_Length(a);
    bool result = (l > 1.0f - epsilon && l < 1.0f + epsilon);
    return result;
}

typedef struct matrix2D
{
    f32 n[2][2];
} matrix2D;

#define m_n00(m) m.n[0][0]
#define m_n10(m) m.n[0][1]
#define m_n20(m) m.n[0][2]
#define m_n30(m) m.n[0][3]
#define m_n01(m) m.n[1][0]
#define m_n11(m) m.n[1][1]
#define m_n21(m) m.n[1][2]
#define m_n31(m) m.n[1][3]
#define m_n02(m) m.n[2][0]
#define m_n12(m) m.n[2][1]
#define m_n22(m) m.n[2][2]
#define m_n32(m) m.n[2][3]
#define m_n03(m) m.n[3][0]
#define m_n13(m) m.n[3][1]
#define m_n23(m) m.n[3][2]
#define m_n33(m) m.n[3][3]

internal inline matrix2D m2x2(f32 n00, f32 n01, f32 n10, f32 n11)
{
    // TODO(torgrim): Check that this is actually valid.
    matrix2D m = {.n = {{n00, n10}, {n01, n11}}};
    return m;
}

internal inline matrix2D m2x2_Mul(matrix2D a, matrix2D b)
{
    matrix2D result = m2x2(m_n00(a) * m_n00(b) + m_n01(a) * m_n10(b),
                           m_n00(a) * m_n01(b) + m_n01(a) * m_n11(b),
                           m_n10(a) * m_n00(b) + m_n11(a) * m_n10(b),
                           m_n10(a) * m_n01(b) + m_n11(a) * m_n11(b));
    return result;
}

// NOTE(torgrim): All matrices are stored column major order eg. m[col][row]
typedef struct matrix3D
{
    f32 n[3][3];
} matrix3D;

internal inline matrix3D m3x3(f32 n00, f32 n01, f32 n02,
                              f32 n10, f32 n11, f32 n12,
                              f32 n20, f32 n21, f32 n22)
{
    matrix3D m;
    m.n[0][0] = n00;
    m.n[0][1] = n10;
    m.n[0][2] = n20;

    m.n[1][0] = n01;
    m.n[1][1] = n11;
    m.n[1][2] = n21;

    m.n[2][0] = n02;
    m.n[2][1] = n12;
    m.n[2][2] = n22;

    return m;
}

internal inline matrix3D m3x3_Mul(matrix3D a, matrix3D b)
{
    matrix3D result = m3x3(
                           m_n00(a) * m_n00(b) + m_n01(a) * m_n10(b) + m_n02(a) * m_n20(b),
                           m_n00(a) * m_n01(b) + m_n01(a) * m_n11(b) + m_n02(a) * m_n21(b),
                           m_n00(a) * m_n02(b) + m_n01(a) * m_n12(b) + m_n02(a) * m_n22(b),

                           m_n10(a) * m_n00(b) + m_n11(a) * m_n10(b) + m_n12(a) * m_n20(b),
                           m_n10(a) * m_n01(b) + m_n11(a) * m_n11(b) + m_n12(a) * m_n21(b),
                           m_n10(a) * m_n02(b) + m_n11(a) * m_n12(b) + m_n12(a) * m_n22(b),

                           m_n20(a) * m_n00(b) + m_n21(a) * m_n10(b) + m_n22(a) * m_n20(b),
                           m_n20(a) * m_n01(b) + m_n21(a) * m_n11(b) + m_n22(a) * m_n21(b),
                           m_n20(a) * m_n02(b) + m_n21(a) * m_n12(b) + m_n22(a) * m_n22(b));
    return result;
}

internal inline v3 m3x3_MulV3(matrix3D m, v3 v)
{
    v3 result = V3(m_n00(m) * v.x + m_n01(m) * v.y + m_n02(m) * v.z,
                   m_n10(m) * v.x + m_n11(m) * v.y + m_n12(m) * v.z,
                   m_n20(m) * v.x + m_n21(m) * v.y + m_n22(m) * v.z);

    return result;
}

internal inline v3 m3x3_GetColumnVector(matrix3D m, u32 col)
{
    tbs_assert(col < 3);

    v3 result =
    {
        .x = m.n[col][0],
        .y = m.n[col][1],
        .z = m.n[col][2],
    };

    return result;
}

typedef struct matrix4D
{
    f32 n[4][4];
} matrix4D;

internal inline matrix4D m4x4(f32 n00, f32 n01, f32 n02, f32 n03,
                              f32 n10, f32 n11, f32 n12, f32 n13,
                              f32 n20, f32 n21, f32 n22, f32 n23,
                              f32 n30, f32 n31, f32 n32, f32 n33)
{
    matrix4D m;
    m.n[0][0] = n00;
    m.n[0][1] = n10;
    m.n[0][2] = n20;
    m.n[0][3] = n30;

    m.n[1][0] = n01;
    m.n[1][1] = n11;
    m.n[1][2] = n21;
    m.n[1][3] = n31;

    m.n[2][0] = n02;
    m.n[2][1] = n12;
    m.n[2][2] = n22;
    m.n[2][3] = n32;

    m.n[3][0] = n03;
    m.n[3][1] = n13;
    m.n[3][2] = n23;
    m.n[3][3] = n33;

    return m;
}


internal inline matrix4D m4x4_Mul(const matrix4D a, const matrix4D b)
{
    matrix4D result = m4x4(
                           m_n00(a) * m_n00(b) + m_n01(a) * m_n10(b) + m_n02(a) * m_n20(b) + m_n03(a) * m_n30(b),
                           m_n00(a) * m_n01(b) + m_n01(a) * m_n11(b) + m_n02(a) * m_n21(b) + m_n03(a) * m_n31(b),
                           m_n00(a) * m_n02(b) + m_n01(a) * m_n12(b) + m_n02(a) * m_n22(b) + m_n03(a) * m_n32(b),
                           m_n00(a) * m_n03(b) + m_n01(a) * m_n13(b) + m_n02(a) * m_n23(b) + m_n03(a) * m_n33(b),

                           m_n10(a) * m_n00(b) + m_n11(a) * m_n10(b) + m_n12(a) * m_n20(b) + m_n13(a) * m_n30(b),
                           m_n10(a) * m_n01(b) + m_n11(a) * m_n11(b) + m_n12(a) * m_n21(b) + m_n13(a) * m_n31(b),
                           m_n10(a) * m_n02(b) + m_n11(a) * m_n12(b) + m_n12(a) * m_n22(b) + m_n13(a) * m_n32(b),
                           m_n10(a) * m_n03(b) + m_n11(a) * m_n13(b) + m_n12(a) * m_n23(b) + m_n13(a) * m_n33(b),

                           m_n20(a) * m_n00(b) + m_n21(a) * m_n10(b) + m_n22(a) * m_n20(b) + m_n23(a) * m_n30(b),
                           m_n20(a) * m_n01(b) + m_n21(a) * m_n11(b) + m_n22(a) * m_n21(b) + m_n23(a) * m_n31(b),
                           m_n20(a) * m_n02(b) + m_n21(a) * m_n12(b) + m_n22(a) * m_n22(b) + m_n23(a) * m_n32(b),
                           m_n20(a) * m_n03(b) + m_n21(a) * m_n13(b) + m_n22(a) * m_n23(b) + m_n23(a) * m_n33(b),

                           m_n30(a) * m_n00(b) + m_n31(a) * m_n10(b) + m_n32(a) * m_n20(b) + m_n33(a) * m_n30(b),
                           m_n30(a) * m_n01(b) + m_n31(a) * m_n11(b) + m_n32(a) * m_n21(b) + m_n33(a) * m_n31(b),
                           m_n30(a) * m_n02(b) + m_n31(a) * m_n12(b) + m_n32(a) * m_n22(b) + m_n33(a) * m_n32(b),
                           m_n30(a) * m_n03(b) + m_n31(a) * m_n13(b) + m_n32(a) * m_n23(b) + m_n33(a) * m_n33(b));

    return result;
}

internal inline matrix4D m4x4_CreateZero(void)
{
    matrix4D result = m4x4(0.0f, 0.0f, 0.0f, 0.0f,
                           0.0f, 0.0f, 0.0f, 0.0f,
                           0.0f, 0.0f, 0.0f, 0.0f,
                           0.0f, 0.0f, 0.0f, 0.0f);

    return result;
}

internal inline matrix4D m4x4_CreateIdentity(void)
{
    matrix4D result = m4x4(1.0f, 0.0f, 0.0f, 0.0f,
                           0.0f, 1.0f, 0.0f, 0.0f,
                           0.0f, 0.0f, 1.0f, 0.0f,
                           0.0f, 0.0f, 0.0f, 1.0f);

    return result;
}


internal inline v4 m4x4_MulV4(const matrix4D m, const v4 v)
{
    v4 result = V4(m_n00(m) * v.x + m_n01(m) * v.y + m_n02(m) * v.z + m_n03(m) * v.w,
                   m_n10(m) * v.x + m_n11(m) * v.y + m_n12(m) * v.z + m_n13(m) * v.w,
                   m_n20(m) * v.x + m_n21(m) * v.y + m_n22(m) * v.z + m_n23(m) * v.w,
                   m_n30(m) * v.x + m_n31(m) * v.y + m_n32(m) * v.z + m_n33(m) * v.w);

    return result;
}

internal inline matrix4D m4x4_Mulf32(const matrix4D m, const f32 s)
{
    matrix4D result = m4x4(m_n00(m) * s, m_n01(m) * s, m_n02(m) * s, m_n03(m) * s,
                           m_n10(m) * s, m_n11(m) * s, m_n12(m) * s, m_n13(m) * s,
                           m_n20(m) * s, m_n21(m) * s, m_n22(m) * s, m_n23(m) * s,
                           m_n30(m) * s, m_n31(m) * s, m_n32(m) * s, m_n33(m) * s);

    return result;
}

// NOTE(torgrim): matrix 3D methods
// TODO(torgrim): not sure about function like this
// that transforms from->to. Should the from type
// come first in the name?
internal inline matrix3D m4x4_to_m3x3(matrix4D m)
{
    matrix3D result = m3x3(m_n00(m), m_n01(m), m_n02(m),
                           m_n10(m), m_n11(m), m_n12(m),
                           m_n20(m), m_n21(m), m_n22(m));

    return result;
}
internal inline matrix3D m3x3_Transpose(matrix3D m)
{
    matrix3D result = m3x3(m_n00(m), m_n10(m), m_n20(m),
                           m_n01(m), m_n11(m), m_n21(m),
                           m_n02(m), m_n12(m), m_n22(m));

    return result;
}


// TODO(torgrim): make this more explicit that
// it's using angles in degree.
internal inline matrix3D m3x3_XRotate(f32 angle)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    matrix3D result = m3x3(1.0f, 0.0f,  0.0f,
                           0.0f, c,    -s,
                           0.0f, s,     c);

    return result;
}

internal inline matrix3D m3x3_YRotate(f32 angle)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    matrix3D result = m3x3( c,       0.0f,   s,
                            0.0f,    1.0f,   0.0f,
                           -s,       0.0f,   c);

    return result;
}

internal inline matrix3D m3x3_ZRotate(f32 angle)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    matrix3D result = m3x3(c,       -s,     0.0f,
                           s,        c,     0.0f,
                           0.0f,     0.0f,  1.0f);

    return result;
}

// TODO(torgrim): make it more explicit which order
// we are applying the rotation and add other options as well
// so that it's easier to see the difference between them
internal inline matrix3D m3x3_CreateRotate3f(f32 angleX, f32 angleY, f32 angleZ)
{
    matrix3D yRot = m3x3_YRotate(angleY);
    matrix3D xRot = m3x3_XRotate(angleX);
    matrix3D zRot = m3x3_ZRotate(angleZ);

    matrix3D result = m3x3_Mul(m3x3_Mul(yRot, xRot), zRot);
    return result;
}

// TODO(torgrim): make it more explicit which order
// we are applying the rotation and add other options as well
// so that it's easier to see the difference between them
internal inline matrix3D m3x3_CreateRotateV3(v3 angles)
{
    matrix3D result = m3x3_CreateRotate3f(angles.x, angles.y, angles.z);
    return result;
}

internal inline matrix3D m3x3_CreateRotateAboutAxis(f32 angle, v3 u)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    f32 t = 1.0f - c;


    matrix3D result = m3x3(t * Pow2(u.x) + c, t*u.x*u.y - s*u.z, t*u.x*u.z + s*u.y,
                           t*u.x*u.y + s*u.z, t * Pow2(u.y) + c, t*u.y*u.z - s*u.x,
                           t*u.x*u.z - s*u.y, t*u.y*u.z + s*u.x, t * Pow2(u.z) + c);

    return result;
}

internal inline matrix3D m3x3_CreateScale3f(f32 x, f32 y, f32 z)
{
    matrix3D result = m3x3(x,       0.0f,   0.0f,
                           0.0f,    y,      0.0f,
                           0.0f,    0.0f,   z);

    return result;
}

internal inline matrix3D m3x3_CreateScaleV3(v3 *v)
{
    matrix3D result = m3x3(v->x,    0.0f,   0.0f,
                           0.0f,    v->y,   0.0f,
                           0.0f,    0.0f,   v->z);

    return result;
}

// NOTE(torgrim): matrix 4D methods
// TODO(torgrim): make it more explicit that we are doing this
// with 4d matrices.
internal inline matrix4D m4x4_Transpose(matrix4D m)
{
    matrix4D result = m4x4(m_n00(m), m_n10(m), m_n20(m), m_n30(m),
                           m_n01(m), m_n11(m), m_n21(m), m_n31(m),
                           m_n02(m), m_n12(m), m_n22(m), m_n32(m),
                           m_n03(m), m_n13(m), m_n23(m), m_n33(m));

    return result;
}


// TODO(torgrim): make this more explicit that
// it's using angles in degree.
internal inline matrix4D m4x4_XRotate(f32 angle)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    matrix4D result = m4x4(1.0f, 0.0f, 0.0f, 0.0f,
                           0.0f, c,    -s,   0.0f,
                           0.0f, s,     c,   0.0f,
                           0.0f, 0.0f, 0.0f, 1.0f);

    return result;
}

internal inline matrix4D m4x4_YRotate(f32 angle)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    matrix4D result = m4x4(c,       0.0f,   s,      0.0f,
                           0.0f,    1.0f,   0.0f,   0.0f,
                           -s,      0.0f,   c,      0.0f,
                           0.0f,    0.0f,   0.0f,   1.0f);

    return result;
}

internal inline matrix4D m4x4_ZRotate(f32 angle)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    matrix4D result = m4x4(c,       -s,     0.0f,   0.0f,
                           s,        c,     0.0f,   0.0f,
                           0.0f,     0.0f,  1.0f,   0.0f,
                           0.0f,     0.0f,  0.0f,   1.0f);

    return result;
}

// TODO(torgrim): make it more explicit which order
// we are applying the rotation and add other options as well
// so that it's easier to see the difference between them
internal inline matrix4D m4x4_CreateRotate3f(f32 angleX, f32 angleY, f32 angleZ)
{
    matrix4D yRot = m4x4_YRotate(angleY);
    matrix4D xRot = m4x4_XRotate(angleX);
    matrix4D zRot = m4x4_ZRotate(angleZ);

    matrix4D result = m4x4_Mul(m4x4_Mul(yRot, xRot), zRot);
    return result;
}

// TODO(torgrim): make it more explicit which order
// we are applying the rotation and add other options as well
// so that it's easier to see the difference between them
internal inline matrix4D m4x4_CreateRotateV3(v3 angles)
{
    matrix4D result = m4x4_CreateRotate3f(angles.x, angles.y, angles.z);
    return result;
}

internal inline matrix4D m4x4_CreateRotateAboutAxis(f32 angle, v3 u)
{
    f32 c = Cos(DegreeToRadians(angle));
    f32 s = Sin(DegreeToRadians(angle));

    f32 t = 1.0f - c;


    matrix4D result = m4x4(t * Pow2(u.x) + c, t*u.x*u.y - s*u.z, t*u.x*u.z + s*u.y, 0,
                           t*u.x*u.y + s*u.z, t * Pow2(u.y) + c, t*u.y*u.z - s*u.x, 0,
                           t*u.x*u.z - s*u.y, t*u.y*u.z + s*u.x, t * Pow2(u.z) + c, 0,
                           0, 0, 0, 1);

    return result;
}

internal inline matrix4D m4x4_CreateTranslate3f(f32 x, f32 y, f32 z)
{
    matrix4D result = m4x4(1.0f, 0.0f, 0.0f, x,
                           0.0f, 1.0f, 0.0f, y,
                           0.0f, 0.0f, 1.0f, z,
                           0.0f, 0.0f, 0.0f, 1.0f);

    return result;
}

// TODO(torgrim): not sure what I like most
// with functions like these? Should we just pass in arguments
// as references instead?
internal inline matrix4D m4x4_CreateTranslateV3(v3 v)
{
    matrix4D result = m4x4(1.0f, 0.0f, 0.0f, v.x,
                           0.0f, 1.0f, 0.0f, v.y,
                           0.0f, 0.0f, 1.0f, v.z,
                           0.0f, 0.0f, 0.0f, 1.0f);

    return result;
}


internal inline matrix4D m4x4_CreateScale3f(f32 x, f32 y, f32 z)
{
    matrix4D result = m4x4(x,       0.0f,   0.0f,   0.0f,
                           0.0f,    y,      0.0f,   0.0f,
                           0.0f,    0.0f,   z,      0.0f,
                           0.0f,    0.0f,   0.0f,   1.0f);

    return result;
}

internal inline matrix4D m4x4_CreateScaleV3(v3 *v)
{
    matrix4D result = m4x4(v->x,    0.0f,   0.0f,    0.0f,
                           0.0f,    v->y,   0.0f,   0.0f,
                           0.0f,    0.0f,   v->z,   0.0f,
                           0.0f,    0.0f,   0.0f,   1.0f);

    return result;
}

// TODO(torgrim): Create correct version of this.
internal inline matrix4D CreateOrtho2D(f32 l, f32 r, f32 b, f32 t)
{
    matrix4D result = m4x4(2.0f / (r-l), 0.0f,          0.0f, -((r+l)/(r-l)),
                           0.0f,         2.0f / (t-b),  0.0f, -((t+b)/(t-b)),
                           0.0f,         0.0f,         -1.0f, 0.0f,
                           0.0f,         0.0f,          0.0f, 1.0f);

    return result;
}

// TODO(torgrim): implement proper support for orthographic 3d viewing
// in the engine.
// TODO(torgrim): Create correct version of this.
internal inline matrix4D CreateOrtho3D(f32 l, f32 r, f32 b, f32 t, f32 n, f32 f)
{
    matrix4D result = m4x4(2.0f / (r-l), 0.0f,          0.0f, 0.0f,
                           0.0f,         2.0f / (t-b),  0.0f, 0.0f,
                           0.0f,         0.0f,         -2.0f / (f-n), -(f+n) / (f-n),
                           0.0f,         0.0f,          0.0f, 1.0f);

    return result;
}

// TODO(torgrim): get a better understanding of how this is built
// and what each index does to a point(vertex).
// TODO(torgrim): this is currently not a rev frustum but
// a normal frustum
internal inline matrix4D CreateRevFrustum(f32 fov_y, f32 s, f32 n, f32 f)
{
    f32 g = 1.0f / Tanf32(fov_y * 0.5f);

    matrix4D result = m4x4(g / s,    0.0f,    0.0f,   0.0f,
                           0.0f,     g,       0.0f,   0.0f,
                           0.0f,     0.0f,   -((f + n) / (f - n)), -((2.0f * f * n) / (f - n)),
                           0.0f,     0.0f,   -1.0f,   0.0f);

    return result;

}

internal inline matrix4D m4x4_InvertTransform(matrix4D m)
{
    v3 a = V3(m_n00(m), m_n10(m), m_n20(m));
    v3 b = V3(m_n01(m), m_n11(m), m_n21(m));
    v3 c = V3(m_n02(m), m_n12(m), m_n22(m));
    v3 d = V3(m_n03(m), m_n13(m), m_n23(m));

    v3 s = v3_Cross(a,b);
    v3 t = v3_Cross(c,d);

    f32 invDet = v3_Dot(s,c);

    s = v3_Mulf32(s, invDet);

    v3 r0 = v3_Mulf32(v3_Cross(b,c), invDet);
    v3 r1 = v3_Mulf32(v3_Cross(c,a), invDet);

    matrix4D result = m4x4(r0.x,    r0.y,   r0.z,   v3_Dot(v3_Neg(b),t),
                           r1.x,    r1.y,   r1.z,   v3_Dot(a,t),
                           s.x,     s.y,    s.z,    v3_Dot(v3_Neg(d),s),
                           0.0f,    0.0f,   0.0f,   1.0f);

    return result;
}

// TODO(torgrim): Handle when up and direction is
// the same vector.
internal inline matrix4D LookAt(v3 origin, v3 target, v3 up)
{
    v3 dir = v3_Normalize(v3_Sub(origin, target));
    tbs_assert(!v3_Equal(dir, up));

    v3 right = v3_Normalize(v3_Cross(up, dir));
    v3 localUp = v3_Normalize(v3_Cross(dir, right));

    matrix4D view = m4x4(right.x,       right.y,        right.z,        0.0f,
                         localUp.x,     localUp.y,      localUp.z,      0.0f,
                         dir.x,         dir.y,          dir.z,          0.0f,
                         0.0f,          0.0f,           0.0f,           1.0f);

    view = m4x4_Mul(view, m4x4_CreateTranslate3f(-origin.x, -origin.y, -origin.z));

    return view;
}

// TODO(torgrim): need to rename this where it's clear what
// this does since it's not really a valid multiplication to
// multiply a 3d vector with a 4x4 matrix
internal inline v3 MultMatrix4D(v3 a, matrix4D m)
{
    v3 result = V3(a.x*m_n00(m) + a.y*m_n01(m) + a.z*m_n02(m),
                   a.x*m_n10(m) + a.y*m_n11(m) + a.z*m_n12(m),
                   a.x*m_n20(m) + a.y*m_n21(m) + a.z*m_n22(m));

    return result;
}

internal inline v3 CalcEulerFromRotation(matrix4D m)
{
    v3 result;
    result.x = RadianToDegree(ArcSin(m_n21(m)));
    result.y = RadianToDegree(ArcTan2(-m_n20(m), m_n22(m)));
    result.z = RadianToDegree(ArcTan2(-m_n01(m), m_n11(m)));

    return result;
}

global const v2 v2_Zero = {0.0f, 0.0f};

global const v3 v3_Zero = {0.0f, 0.0f, 0.0f};
global const v3 v3_XAxis = {1.0f, 0.0f, 0.0f};
global const v3 v3_YAxis = {0.0f, 1.0f, 0.0f};
global const v3 v3_ZAxis = {0.0f, 0.0f, 1.0f};

#define TBS_MATH_H

#endif
