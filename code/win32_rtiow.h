#ifndef WIN32_RTIOW_H

// TODO(torgrim): enum instead?
#define M_LAMBERTIAN 1
#define M_METAL 2
#define M_DIELECTRIC 3

typedef struct file_data
{
    u8 *content;
    size_t length;
} file_data;

typedef struct line_info
{
    u8 *start;
    size_t length;
} line_info;

typedef struct mouse_input
{
    v2 delta;
    v2 lastLeftUpPosClient;
    bool leftButtonDown;
    bool rightButtonDown;
    bool middleButtonDown;
    bool wheelDelta;

} mouse_input;

typedef struct keyboard_input
{
    u64 frameStates;
    u64 prevStates;
    u64 buttonDowns;
    u64 buttonUps;
    bool isDown;
} keyboard_input;

typedef struct input_state
{
    keyboard_input keyboard;
    mouse_input mouse;
} input_state;

typedef struct simple_camera
{
    v3 position;
    v3 rotation;
    f32 fov;
    f32 aperture;
    f32 focusDist;
    f32 speed;
    v3 rotationSpeed;
} simple_camera;

typedef enum compute_mode
{
    COMPUTE_MODE_None,
    COMPUTE_MODE_RayCast,
    COMPUTE_MODE_Scatter,
    COMPUTE_MODE_Render,
    COMPUTE_MODE_CameraRaysInit,
    COMPUTE_MODE_SampleInit,
    COMPUTE_MODE_SampleEnd,
    COMPUTE_MODE_Simple,
    COMPUTE_MODE_CollapsedCastScatter,
} compute_mode;

typedef enum viewer_mode
{
    VIEWER_MODE_None,
    VIEWER_MODE_FlyCamera,
    VIEWER_MODE_FullRender,
    VIEWER_MODE_RenderDone,
} viewer_mode;

typedef struct application_context
{
    f32 runtime;
    f32 dt;
    u32 workGroupCountX;
    u32 workGroupCountY;
    bool useMotionBlur;
    i32 currentSampleCount;
    viewer_mode viewerMode;
    compute_mode initMode;
    u32 targetVAOID;
    u32 targetVBOID;
    bool regenerateNoiseTexture;
} application_context;

// NOTE(torgrim): This is shader storage buffer objects with layout std140
// and therefore need to adhere to those rules.
typedef struct pixel_info
{
    v3 ray_origin;
    GLint padding_0;
    v3 ray_dir;
    GLint padding_1;
    v3 sample_color;
    GLint padding_2;
    v3 final_color;
    // NOTE(torgrim): Boolean values
    GLint was_hit;
    GLint finished;
    GLfloat depth;
    GLint hit_id;
    GLuint rng;
    GLfloat time;
    GLuint scatter_count;

    char padding_3[10];
    // size = 86
    // alignment need = 16
} pixel_info;


internal f32 GetRandom01(u32 *state);
internal FILETIME Win32GetLastWriteTime(const char *filename);
internal file_data Win32ReadFile(const char *fileName, bool zeroTerminate);
internal file_data Win32ReadAllText(const char *filename);
internal bool Win32FreeFileInfo(file_data *fileInfo);

internal line_info GetNextLine(u8 *cur);
internal bool LineEqual(line_info line, u8 *match, size_t matchLength);

internal char *tbs_CopyString(const char *s);

#define WIN32_RTIOW_H
#endif
