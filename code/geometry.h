#ifndef GEOMETRY_H
// NOTE(torgrim): This has to be compatible with std140 layout of GLSL
// We can not use std430 because uniform blocks doesn't support that layout.
// (should test this). We might consider just using
// an ssbo instead here so that we can use std430 if it doesn't degrade the
// performance a lot.
// TODO(torgrim): Change to use application convention instead
// of GLSL convention(snake_case vs camelCase)
typedef struct sphere
{
    v4 pos;
    v4 base_color;
    v4 emit;
    v4 start_pos;
    v4 end_pos;
    f32 radius;
    i32 material_type;
    f32 fuzz;
    f32 ref_index;
    i32 id;

    v3 padding;
} sphere;

typedef struct rect_data
{
    v4 v0;
    v4 v1;
    v4 v2;
    v4 base_color;
    v3 emit;
    f32 radius;
    i32 material_type;
    f32 fuzz;
    f32 ref_index;
    i32 id;
    i32 useTexture;

    v3 padding;
} rect_data;

typedef struct sphere_list
{
    sphere *data;
    u32 count;
} sphere_list;

typedef struct rect_list
{
    rect_data *data;
    u32 count;
} rect_list;

#define GEOMETRY_H
#endif
