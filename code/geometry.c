#include "geometry.h"

internal void RotateAABox(rect_data *mem, v3 angles)
{
    rect_data pivot = *mem;
    // TODO(torgrim): both depth and width can have different offset.
    f32 offset = (pivot.v1.x - pivot.v0.x) / 2.0f;
    for(size_t i = 0; i < 6; ++i)
    {
        rect_data *rect = mem + i;
        matrix4D rot = m4x4_CreateRotateV3(angles);
        matrix4D tra0 = m4x4_CreateTranslate3f((-pivot.v0.x) - offset, 0.0f, (-pivot.v0.z) + offset);
        matrix4D tra1 = m4x4_CreateTranslate3f(pivot.v0.x + offset, 0.0f,  pivot.v0.z - offset);
        rot = m4x4_Mul(rot, tra0);
        rot = m4x4_Mul(tra1, rot);
        rect->v0 = m4x4_MulV4(rot, rect->v0);
        rect->v1 = m4x4_MulV4(rot, rect->v1);
        rect->v2 = m4x4_MulV4(rot, rect->v2);
    }
}

internal void AddAABox(rect_data *mem, v3 pos, f32 width, f32 height, f32 depth, bool inwardNormals, i32 idOffset, int materialType, v3 emit, bool useTexture)
{
    v4 lowerLeftFront;
    v4 lowerRightFront;
    v4 topLeftFront;
    v4 topRightFront;
    v4 lowerLeftBack;
    v4 lowerRightBack;
    v4 topLeftBack;
    v4 topRightBack;

    if(inwardNormals == false)
    {
        lowerLeftFront = v3_to_v4(pos);
        lowerLeftFront.w = 1.0f;

        lowerRightFront = lowerLeftFront;
        lowerRightFront.x += width;

        topLeftFront = lowerLeftFront;
        topLeftFront.y += height;

        topRightFront = lowerRightFront;
        topRightFront.y += height;

        lowerLeftBack = lowerLeftFront;
        lowerLeftBack.z -= depth;

        lowerRightBack = lowerLeftBack;
        lowerRightBack.x += width;

        topLeftBack = lowerLeftBack;
        topLeftBack.y += height;

        topRightBack = lowerRightBack;
        topRightBack.y += height;
    }
    else
    {
        lowerRightFront = v3_to_v4(pos);
        lowerRightFront.w += 1.0f;

        lowerLeftFront = lowerRightFront;
        lowerLeftFront.x += width;

        topLeftFront = lowerLeftFront;
        topLeftFront.y += height;

        topRightFront = lowerRightFront;
        topRightFront.y += height;

        lowerLeftBack = lowerLeftFront;
        lowerLeftBack.z -= depth;

        lowerRightBack = lowerLeftBack;
        lowerRightBack.x -= width;

        topLeftBack = lowerLeftBack;
        topLeftBack.y += height;

        topRightBack = lowerRightBack;
        topRightBack.y += height;
    }

    // front
    rect_data *front = mem;
    front->v0 = lowerLeftFront;
    front->v1 = lowerRightFront;
    front->v2 = topLeftFront;
    front->radius = 1.0f;
    front->material_type = materialType;
    front->base_color = V4(0.73f, 0.73f, 0.73f, 0.0f);
    front->emit = emit;
    front->ref_index = 1.5f;
    front->fuzz = 0.0f;
    front->id = idOffset;
    front->useTexture = useTexture;
    ++idOffset;

    // left
    rect_data *left = mem + 1;
    left->v0 = lowerLeftBack;
    left->v1 = lowerLeftFront;
    left->v2 = topLeftBack;
    left->radius = 1.0f;
    left->material_type = materialType;
    left->base_color = V4(0.73f, 0.73f, 0.73f, 0.0f);
    left->emit = emit;
    left->id = idOffset;
    left->useTexture = useTexture;
    ++idOffset;

    // right
    rect_data *right = mem + 2;
    right->v0 = lowerRightFront;
    right->v1 = lowerRightBack;
    right->v2 = topRightFront;
    right->radius = 1.0f;
    right->material_type = materialType;
    right->base_color = V4(0.73f, 0.73f, 0.73f, 0.0f);
    right->emit = emit;
    right->id = idOffset;
    right->useTexture = useTexture;
    ++idOffset;

    // back
    rect_data *back = mem + 3;
    back->v0 = lowerRightBack;
    back->v1 = lowerLeftBack;
    back->v2 = topRightBack;
    back->radius = 1.0f;
    back->material_type = materialType;
    back->base_color = V4(0.73f, 0.73f, 0.73f, 0.0f);
    back->emit = emit;
    back->id = idOffset;
    back->useTexture = useTexture;
    ++idOffset;

    // top
    rect_data *top = mem + 4;
    top->v0 = topLeftFront;
    top->v1 = topRightFront;
    top->v2 = topLeftBack;
    top->radius = 1.0f;
    top->material_type = materialType;
    top->base_color = V4(0.73f, 0.73f, 0.73f, 0.0f);
    top->emit = emit;
    top->ref_index = 1.5f;
    top->id = idOffset;
    top->useTexture = useTexture;
    ++idOffset;

    // bottom
    rect_data *bottom = mem + 5;
    bottom->v0 = lowerLeftBack;
    bottom->v1 = lowerRightBack;
    bottom->v2 = lowerLeftFront;
    bottom->radius = 1.0f;
    bottom->material_type = materialType;
    bottom->base_color = V4(0.73f, 0.73f, 0.73f, 0.0f);
    bottom->emit = emit;
    bottom->id = idOffset;
    bottom->useTexture = useTexture;
    ++idOffset;
}

