#version 450 core

layout(local_size_x = 1, local_size_y = 1) in;

layout(rgba32f, binding = 1) uniform image2D result_image;

uniform float iTime;

float random_white(vec2 uv)
{
    //float value = fract(sin(dot(uv, vec2(12.9898f, 78.233f))) * 1437578.5453f * sin(iTime));
    float value = fract(sin(dot(uv, vec2(12.9898f, 78.233f))) * 1437578.5453f);
    return value;
}

vec3 generate_white_noise(vec2 uv)
{
    vec2 scale_pos = floor(uv*1000.0f);
    float value = random_white(scale_pos);
    vec3 emit;
#if 0
    if(value > 0.93)
    {
        emit = vec3(10.0f, 0.0f, 0.0f);
        value = 10.0f;
    }
    else if(value > 0.75f)
    {
        emit = vec3(0.0f, 10.0f, 0.0f);
        value = 0.0f;
    }
    else if(value > 0.5f)
    {
        emit = vec3(0.0f, 0.0f, 1.0f);
        value = 0.0f;
    }
    else
    {
        value = 0.0f;
        emit = vec3(0.0f);
    }
#endif
    vec3 result = vec3(value*1.0f) * vec3(0.761f, 0.698f, 0.502f);
    //vec3 result = emit;

    return result;
}

void main()
{
    ivec2 pixel = ivec2(gl_GlobalInvocationID.x, gl_GlobalInvocationID.y);
    vec2 res = imageSize(result_image);
    vec2 uv = vec2(pixel) / res;
    vec3 result_noise = generate_white_noise(uv);
    imageStore(result_image, pixel, vec4(result_noise, 1.0f));
}
