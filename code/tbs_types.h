/* date = June 3rd 2020 4:45 pm */

#ifndef TBS_TYPES_H

#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#define tbs_assert(expr) assert(expr)
#define ARRAY_COUNT(array) (sizeof(array) / sizeof(array[0]))

#define KB(value) 1024ULL*value;
#define MB(value) 1024ULL*KB(value);
#define GB(value) 1024ULL*MB(value);

typedef int8_t i8;
typedef uint8_t u8;
typedef int16_t i16;
typedef uint16_t u16;
typedef int32_t i32;
typedef uint32_t u32;
typedef int64_t i64;
typedef uint64_t u64;

typedef float f32;
typedef double f64;

#define internal static
#define local_persist static
#define global static

#define MAX_DEBUG_TEXT_SIZE 512
internal void PrintDebugText(const char *fmt, ...)
{
    va_list argList;
    va_start(argList, fmt);
    char outBuff[MAX_DEBUG_TEXT_SIZE];
    vsnprintf(outBuff, MAX_DEBUG_TEXT_SIZE, fmt, argList);
    if(IsDebuggerPresent())
    {
        OutputDebugString(outBuff);
    }
    else
    {
        printf("%s", outBuff);
    }
}

#define TBS_TYPES_H

#endif //TBS_TYPES_H
