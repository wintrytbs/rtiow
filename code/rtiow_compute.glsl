#version 450 core

#define FLT_MAX 3.402823466e+38F
#define PI 3.1415926535F

layout(local_size_x = 8, local_size_y = 8) in;

layout(rgba32f, binding = 0) uniform image2D result_image;
layout(binding = 1) uniform sampler2D test_texture;

#define M_LAMBERTIAN 1
#define M_METAL 2
#define M_DIELECTRIC 3

#define COMPUTE_MODE_None 0
#define COMPUTE_MODE_RayCast 1
#define COMPUTE_MODE_Scatter 2
#define COMPUTE_MODE_Render 3
#define COMPUTE_MODE_CameraRaysInit 4
#define COMPUTE_MODE_SampleInit 5
#define COMPUTE_MODE_SampleEnd 6
#define COMPUTE_MODE_Simple 7
#define COMPUTE_MODE_CollapsedCastScatter 8

#define OBJECT_TYPE_Sphere 0
#define OBJECT_TYPE_Rectangle 1

struct rect_prim
{
    vec3 v0;
    vec3 v1;
    vec3 v2;
    vec3 base_color;
    vec3 emit;
    float radius;
    int material_type;
    float fuzz;
    float ref_index;
    int id;
    bool use_texture;
};

struct sphere
{
    vec4 pos;
    vec4 base_color;
    vec4 emit;
    vec4 start_pos;
    vec4 end_pos;
    float radius;
    int material_type;
    float fuzz;
    float ref_index;
    int id;
};

struct pixel_info
{
    vec3 ray_origin;
    vec3 ray_dir;
    vec3 sample_color;
    vec3 final_color;
    bool any_hit;
    bool finished;
    float depth;
    int hit_id;
    uint rng;
    float time;
    uint scatter_count;
};

uniform float iTime;
uniform int process_mode;
uniform int current_sample_index;
uniform vec3 cam_pos;
uniform float cam_aperture;
uniform vec2 image_plane;
uniform mat4 view;
uniform bool use_motion_blur;
uniform bool use_sky_background;

layout(std140, binding = 2) buffer full_sphere_list
{
    sphere all_spheres[];
};

layout(std140, binding = 3) buffer pixel_info_buffer
{
    pixel_info pixel_data[];
};

layout(std140, binding = 4) buffer full_rect_list
{
    rect_prim all_rects[];
};

// NOTE(torgrim): From Nathan Reed http://www.reedbeta.com/blog/quick-and-easy-gpu-random-numbers-in-d3d11/
uint wang_hash(inout uint seed)
{
    seed = uint(seed ^ uint(61)) ^ uint(seed >> uint(16));
    seed *= uint(9);
    seed = seed ^ (seed >> 4);
    seed *= uint(0x27d4eb2d);
    seed = seed ^ (seed >> 15);
    return seed;
}

float random_float_01(inout uint state)
{
    return float(wang_hash(state)) * (1.0f / 4294967296.0f);
}

uint create_rng_state(uint a, uint b)
{
    //uint rng_state = 1664525u * a + 1013904223u * + b;
    uint rng_state = (a * 1973u + b + 9277u + 26699u) | 1u;
    return rng_state;
}

float length_squared(vec3 a)
{
    float result = (a.x*a.x + a.y*a.y + a.z*a.z);
    return result;
}

vec3 sample_sphere_texture(in vec3 hit_pos, in sampler2D tex)
{
    // TODO(torgrim): atan is not defined when z == 0;
    float theta = atan(hit_pos.x, hit_pos.z);
    float phi = asin(hit_pos.y);

    float u = (theta+PI) / (2.0f*PI);
    float v = (phi + (PI*0.5f)) / PI;
    vec4 sam = texture(tex, vec2(u, v));
    return vec3(sam);
}

vec3 sample_rect_texture(in rect_prim rect, in vec3 hit_pos, in sampler2D tex)
{
    vec3 local_hit_pos = hit_pos - rect.v0;
    vec3 edge0 = rect.v1 - rect.v0;
    vec3 edge1 = rect.v2 - rect.v0;

    vec2 uv = vec2(dot(local_hit_pos, edge0) / (length(edge0)*length(edge0)), dot(local_hit_pos, edge1) / (length(edge1)*length(edge1)));
    vec4 sam = texture(tex, uv);
    return vec3(sam);
}

vec3 random_in_unit_disk(inout uint rng_state)
{
    vec3 p;
    do
    {
        float random_y = random_float_01(rng_state);
        float random_x = random_float_01(rng_state);
        p = 2.0f * vec3(random_x, random_y, 0.0f) - vec3(1, 1, 0);
    } while(length_squared(p) >= 1.0f);

    return p;
}

vec3 random_in_unit_sphere(inout uint rng_state)
{
    vec3 p;
    do
    {
        float random_y = random_float_01(rng_state);
        float random_x = random_float_01(rng_state);
        float random_z = random_float_01(rng_state);
        p = 2.0f * vec3(random_x, random_y, random_z) - vec3(1, 1, 1);
    } while(length_squared(p) >= 1.0f);
    return p;
}

float schlickApprox(float c, float ref_index)
{
    float r0 = (1.0f-ref_index) / (1.0f+ref_index);
    r0 = r0*r0;
    return r0 + (1.0f - r0)*pow((1.0f - c), 5);
}

bool rect_hit(vec3 o, vec3 r, rect_prim rect, inout float closest_t)
{
    vec3 edge0 = rect.v1 - rect.v0;
    vec3 edge1 = rect.v2 - rect.v0;
    vec3 n = normalize(cross(edge0, edge1));
    float den = dot(-r, n);
    if(den > 1e-2)
    {
        vec3 rect_to_o = (o - rect.v0);
        float t = dot(rect_to_o, n) / den;
        if(t >= 0.0f && t < closest_t)
        {
#if 1
            vec3 world_hit_point = (o + t*r);
            vec3 rect_plane_hit_point = (world_hit_point - rect.v0);
            float x_proj = dot(rect_plane_hit_point, edge0);
            float y_proj = dot(rect_plane_hit_point, edge1);
            if(x_proj >= 0 && x_proj <= dot(edge0, edge0) &&
               y_proj >= 0 && y_proj <= dot(edge1, edge1))
            {
                closest_t = t;
                return true;
            }
#else
            closest_t = t;
            return true;
#endif
        }
    }

    return false;
}

bool sphere_hit(vec3 r_o, vec3 r_d, vec3 s_pos, float s_radius, inout float closest_t)
{
    vec3 oc = r_o - s_pos;

    // TODO(torgrim): This can be calculated only once instead of each hit check.
    float a = dot(r_d, r_d);

    float b = dot(oc, r_d);
    float c = dot(oc, oc) - (s_radius*s_radius);
    float d = b*b - a*c;
    if(d > 0)
    {
        float t = (-b - sqrt(d)) / (a);
        if(t < closest_t && t > 1e-2)
        {
            closest_t = t;
            return true;
        }
        t = (-b + sqrt(d)) / (a);
        if(t < closest_t && t > 1e-2f)
        {
            closest_t = t;
            return true;
        }
    }

    return false;
}

bool scatter(int material_type, float fuzz, float ref_index, vec3 ray_d, vec3 p, vec3 n, inout uint rng_state, out vec3 scatter_ray)
{
    if(material_type == M_LAMBERTIAN)
    {
        vec3 target_point = (p + n + random_in_unit_sphere(rng_state));
        scatter_ray = (target_point - p);
        return true;
    }
    else if(material_type == M_METAL)
    {
        scatter_ray = reflect(normalize(ray_d), n) + fuzz*random_in_unit_sphere(rng_state);
        return (dot(scatter_ray, n) > 0);
    }
    else if(material_type == M_DIELECTRIC)
    {
        float ni_over_no = 0.0f;
        vec3 orig_n = n;
        float d = dot(ray_d, n) / length(ray_d);
        float c = d;
        float reflect_prob;
        if(dot(ray_d, n) > 0)
        {
            n = -n;
            ni_over_no = ref_index;
            c = ref_index * d;
        }
        else
        {
            ni_over_no = 1.0f / ref_index;
            c = -d;
        }

        scatter_ray = refract(ray_d, n, ni_over_no);
        if(scatter_ray.x != 0 || scatter_ray.y != 0 || scatter_ray.z != 0)
        {
            reflect_prob = schlickApprox(c, ref_index);
            if(random_float_01(rng_state) < reflect_prob)
            {
                scatter_ray = reflect(ray_d, n);
            }
        }
        else
        {
            scatter_ray = reflect(ray_d, n);
        }

        return true;
    }

    return false;
}

vec3 sample_ray(vec3 ray_origin, vec3 ray_dir, inout uint rng_state, int bounce_count)
{
    vec3 color = vec3(1.0f, 1.0f, 1.0f);
    vec3 albedo;
    int hit_id = 0;
    for(int bIndex = 0; bIndex < bounce_count; ++bIndex)
    {
        bool any_hit = false;
        float closest_t = FLT_MAX;
        uint hit_type = OBJECT_TYPE_Sphere;
        for(int i = 0; i < all_spheres.length(); ++i)
        {
            sphere s = all_spheres[i];
            vec3 sphere_pos = s.pos.xyz;
            float sphere_radius = s.radius;
            bool hit = sphere_hit(ray_origin, ray_dir, sphere_pos, sphere_radius, closest_t);
            if(hit)
            {
                any_hit = true;
                hit_id = i;
            }
        }

        for(int i = 0; i < all_rects.length(); ++i)
        {
            rect_prim rect = all_rects[i];
            bool hit = rect_hit(ray_origin, ray_dir, rect, closest_t);
            if(hit)
            {
                any_hit = true;
                hit_id = i;
                hit_type = OBJECT_TYPE_Rectangle;
            }
        }

        if(any_hit)
        {
            // TODO(torgrim): For dielectrics the albedo == 1 since it just reflect or refracts.
            vec3 target;
            vec3 hit_point = ray_origin + (ray_dir*closest_t);
            vec3 hit_normal;
            int mat_type;
            float fuzz;
            float ref_index;
            vec3 albedo;
            vec3 emit_color = vec3(0.0f, 0.0f, 0.0f);
            if(hit_type == OBJECT_TYPE_Sphere)
            {
                sphere s = all_spheres[hit_id];
                vec3 sphere_pos = vec3(s.pos);
                hit_normal = normalize(hit_point - sphere_pos);
                albedo = vec3(s.base_color);
                mat_type = s.material_type;
                fuzz = s.fuzz;
                ref_index = s.ref_index;
                emit_color = vec3(s.emit);
            }
            else if(hit_type == OBJECT_TYPE_Rectangle)
            {
                rect_prim rect = all_rects[hit_id];
                vec3 edge0 = rect.v1 - rect.v0;
                vec3 edge1 = rect.v2 - rect.v0;
                hit_normal = normalize(cross(edge0, edge1));
                albedo = rect.base_color;
                emit_color = rect.emit;
                mat_type = rect.material_type;
                fuzz = rect.fuzz;
                ref_index = rect.ref_index;
            }

            if(length(emit_color) == 0.0f && scatter(mat_type, fuzz, ref_index, ray_dir, hit_point, hit_normal, rng_state, target))
            {
                ray_origin = hit_point;
                ray_dir = normalize(target);
                color *= albedo;
            }
            else
            {
                color *= emit_color;
                break;
            }
        }
        else
        {
            if(use_sky_background)
            {
                vec3 u_dir = ray_dir;
                float t = 0.5f*(u_dir.y + 1.0f);
                vec3 A = vec3(1, 1, 1);
                vec3 B = vec3(0.5f, 0.7f, 1.0f);
                color *= ((1-t)*A + (t*B));
            }
            else
            {
                color = vec3(0.0f, 0.0f, 0.0f);
            }

            break;
        }
    }

    return color;
}

void main()
{

    uint xPixel = gl_GlobalInvocationID.x;
    uint yPixel = gl_GlobalInvocationID.y;
    ivec2 pixel = ivec2(xPixel, yPixel);
    ivec2 img_dim = imageSize(result_image);

    vec2 img_dimf = vec2(img_dim);
    vec2 pixel_posf = vec2(pixel.x, pixel.y);

    vec3 color = vec3(1.0f, 0.0f, 0.0f);

    if(process_mode == COMPUTE_MODE_CameraRaysInit)
    {
        uint rng_state = create_rng_state(pixel.x, pixel.y);

        vec2 p_offset = pixel_posf + vec2(random_float_01(rng_state), random_float_01(rng_state));
        vec2 uv = p_offset / img_dimf;

        vec2 uv_neg_1_to_1 = vec2(uv * 2.0f - 1.0f);
        float xPos = uv_neg_1_to_1.x * image_plane.x;
        float yPos = uv_neg_1_to_1.y * image_plane.y;

        vec3 imagePlanePos = vec3(xPos, yPos, -1);
        vec3 imagePlanePosW = vec3(view * vec4(imagePlanePos, 1.0f));
        vec3 rayOriginW = cam_pos;

        vec3 ray_dir = normalize(imagePlanePosW - rayOriginW);

        pixel_info pi;
        pi.depth = FLT_MAX;
        pi.ray_origin = rayOriginW;
        pi.ray_dir = ray_dir;
        pi.hit_id = 0;
        pi.any_hit = false;
        pi.finished = false;
        pi.sample_color = vec3(1.0f);
        pi.final_color = vec3(0.0f);
        pi.rng = rng_state;
        pi.scatter_count = 0;
        if(use_motion_blur)
        {
            pi.time = random_float_01(pi.rng);
        }
        else
        {
            pi.time = 0.0f;
        }

        pixel_data[pixel.y * img_dim.x + pixel.x] = pi;

    }
    else if(process_mode == COMPUTE_MODE_SampleInit)
    {
        pixel_info old_pi = pixel_data[pixel.y * img_dim.x + pixel.x];
        uint rng_test = old_pi.rng;
        vec2 p_offset = pixel_posf + vec2(random_float_01(old_pi.rng), random_float_01(old_pi.rng));
        vec2 uv = p_offset / img_dimf;

        vec2 uv_neg_1_to_1 = vec2(uv * 2.0f - 1.0f);
        float xPos = uv_neg_1_to_1.x * image_plane.x;
        float yPos = uv_neg_1_to_1.y * image_plane.y;

        vec3 imagePlanePos = vec3(xPos, yPos, -1);
        vec3 imagePlanePosW = vec3(view * vec4(imagePlanePos, 1.0f));
        vec3 rayOriginW = cam_pos;

        vec3 ray_dir = normalize(imagePlanePosW - rayOriginW);

        pixel_info pi;
        pi.depth = 0;
        pi.ray_origin = rayOriginW;
        pi.ray_dir = ray_dir;
        pi.hit_id = 0;
        pi.any_hit = false;
        pi.finished = false;
        pi.sample_color = vec3(1.0f);
        pi.final_color = old_pi.final_color;
        pi.rng = old_pi.rng;
        pi.scatter_count = 0;
        if(use_motion_blur)
        {
            pi.time = random_float_01(pi.rng);
        }
        else
        {
            pi.time = 0.0f;
        }

        pixel_data[pixel.y * img_dim.x + pixel.x] = pi;

    }
    else if(process_mode == COMPUTE_MODE_CollapsedCastScatter)
    {
        pixel_info old_pi = pixel_data[pixel.y * img_dim.x + pixel.x];
        if(!old_pi.finished)
        {
            float closest_t = FLT_MAX;
            if(old_pi.depth != 0)
            {
                closest_t = old_pi.depth;
            }
            vec3 ray_dir = old_pi.ray_dir;
            vec3 ray_origin = old_pi.ray_origin;
            bool any_hit = false;
            int object_id;
            uint hit_type = 0;
            for(int i = 0; i < all_spheres.length(); ++i)
            {
                sphere s = all_spheres[i];
                vec3 sphere_pos = mix(s.start_pos.xyz, s.end_pos.xyz, old_pi.time);
                float sphere_radius = all_spheres[i].radius;
                bool hit = sphere_hit(ray_origin, ray_dir, sphere_pos, sphere_radius, closest_t);
                if(hit)
                {
                    any_hit = true;
                    object_id = all_spheres[i].id;
                }
            }

            for(int i = 0; i < all_rects.length(); ++i)
            {
                rect_prim rect = all_rects[i];
                bool hit = rect_hit(ray_origin, ray_dir, rect, closest_t);
                if(hit)
                {
                    any_hit = true;
                    object_id = all_rects[i].id;
                    hit_type = OBJECT_TYPE_Rectangle;
                }
            }

            pixel_info pi = old_pi;
            if(any_hit)
            {
                bool do_scatter = false;
                vec3 hit_pos = ray_origin + (ray_dir*closest_t);
                vec3 scatter_ray;
                vec3 scatter_color;
                vec3 emit_color;
                if(hit_type == OBJECT_TYPE_Sphere)
                {
                    sphere s = all_spheres[object_id];
                    vec3 sphere_pos = mix(s.start_pos.xyz, s.end_pos.xyz, pi.time);
                    vec3 hit_normal = normalize(hit_pos - sphere_pos);
                    // TODO(torgrim): Better emit check.
                    do_scatter = (length(s.emit) == 0 && scatter(s.material_type, s.fuzz, s.ref_index, ray_dir, hit_pos, hit_normal, pi.rng, scatter_ray));
                    scatter_color = vec3(s.base_color);
                    emit_color = vec3(s.emit);
                }
                else if(hit_type == OBJECT_TYPE_Rectangle)
                {
                    rect_prim rect = all_rects[object_id];
                    vec3 edge0 = rect.v1 - rect.v0;
                    vec3 edge1 = rect.v2 - rect.v0;
                    vec3 hit_normal = normalize(cross(edge0, edge1));
                    // TODO(torgrim): Better emit check.
                    do_scatter = (length(rect.emit) == 0 && scatter(rect.material_type, rect.fuzz, rect.ref_index, ray_dir, hit_pos, hit_normal, pi.rng, scatter_ray));
                    if(rect.use_texture == false)
                    {
                        scatter_color = rect.base_color;
                        emit_color = rect.emit;
                    }
                    else
                    {
                        scatter_color = sample_rect_texture(rect, hit_pos, test_texture);
                    }
                }

                if(do_scatter)
                {
                    pixel_data[pixel.y * img_dim.x + pixel.x].ray_dir = normalize(scatter_ray);
                    pixel_data[pixel.y * img_dim.x + pixel.x].ray_origin = hit_pos;
                    pixel_data[pixel.y * img_dim.x + pixel.x].depth = 0.0f;
                    pixel_data[pixel.y * img_dim.x + pixel.x].any_hit = false;
                    pixel_data[pixel.y * img_dim.x + pixel.x].sample_color = pi.sample_color * scatter_color;
                    pixel_data[pixel.y * img_dim.x + pixel.x].scatter_count += 1;
                    // NOTE(torgrim): When using textures
                    //pixel_data[pixel.y * img_dim.x + pixel.x].sample_color = pi.sample_color * sample_sphere_texture(hit_normal, test_texture);
                    pixel_data[pixel.y * img_dim.x + pixel.x].rng = pi.rng;
                }
                else
                {
                    pixel_data[pixel.y * img_dim.x + pixel.x].finished = true;
                    pixel_data[pixel.y * img_dim.x + pixel.x].rng = pi.rng;
                    if(pi.scatter_count != 0)
                    {
                        pixel_data[pixel.y * img_dim.x + pixel.x].sample_color *= emit_color;
                    }
                    else
                    {
                        pixel_data[pixel.y * img_dim.x + pixel.x].sample_color = emit_color;
                    }
                }
            }
            else
            {
                vec3 back_color = vec3(0.0f, 0.0f, 0.0f);
                if(use_sky_background)
                {
                    vec3 u_dir = ray_dir;
                    float t = 0.5f*(u_dir.y + 1.0f);
#if 0
                    vec3 A = vec3(1, 1, 1);
                    vec3 B = vec3(0.5f, 0.7f, 1.0f);
#elif 0
                    vec3 A = vec3(0xe1, 0x85, 0x15);
                    A /= 255.0f;
                    vec3 B = vec3(0x16, 0x49, 0x8a);
                    B /= 255.0f;
#elif 0
                    vec3 A = vec3(0xf2, 0x67, 0x1f);
                    A /= 255.0f;
                    vec3 B = vec3(0x16, 0x0a, 0x47);
                    B /= 255.0f;
#elif 0
                    vec3 A = vec3(0xfd, 0xf9, 0x9f);
                    A /= 255.0f;
                    vec3 B = vec3(0x04, 0x1e, 0x42);
                    B /= 255.0f;
#elif 0
                    // Very Nice
                    vec3 A = vec3(0xf4, 0x63, 0x3c);
                    A /= 255.0f;
                    vec3 B = vec3(0x15, 0x0e, 0x5c);
                    B /= 255.0f;
#elif 0
                    vec3 A = vec3(0xbf, 0x3f, 0x76);
                    A /= 255.0f;
                    vec3 B = vec3(0x02, 0x8a, 0x9b);
                    B /= 255.0f;
#elif 0
                    vec3 A = vec3(0x00, 0x2e, 0x5d);
                    A /= 255.0f;
                    vec3 B = vec3(0x00, 0x0d, 0x30);
                    B /= 255.0f;
#else
                    vec3 A = vec3(0xf7, 0xa5, 0x5d);
                    A /= 255.0f;
                    vec3 B = vec3(0x3c, 0x47, 0xc6);
                    B /= 255.0f;
#endif
                    back_color = ((1-t)*A + (t*B));
                }

                pixel_data[pixel.y * img_dim.x + pixel.x].sample_color *= back_color;
                pixel_data[pixel.y * img_dim.x + pixel.x].finished = true;
            }
        }
    }
    else if(process_mode == COMPUTE_MODE_SampleEnd)
    {
        pixel_info pi = pixel_data[pixel.y * img_dim.x + pixel.x];
        pixel_data[pixel.y * img_dim.x + pixel.x].final_color += pi.sample_color;
    }
    else if(process_mode == COMPUTE_MODE_Render)
    {
        pixel_info pi = pixel_data[pixel.y * img_dim.x + pixel.x];
        color = pi.final_color / float(max(current_sample_index, 1));
    }
    else if(process_mode == COMPUTE_MODE_Simple)
    {
        vec3 albedo = vec3(0.0f);
        pixel_info pi = pixel_data[pixel.y * img_dim.x + pixel.x];
        albedo = sample_ray(pi.ray_origin, pi.ray_dir, pi.rng, 4);
        pixel_data[pixel.y * img_dim.x + pixel.x].final_color = albedo;
    }

    imageStore(result_image, pixel, vec4(color, 1.0f));
}
