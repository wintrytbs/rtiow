# RTIOW

This is a simple path tracer based on the first two books in Peter Shirley's ray tracing intro series.  
The project uses OpenGL 4.3 to render and the path tracing is done by a GLSL compute shader.  
It was used as an introduction to both OpenGL compute shaders and path tracing.  
A lot of optimizations and improvements could be implemented in this project but they will
be prioritized in a [new project](https://gitlab.com/wintrytbs/nordlys) instead with a more complete path tracer. 

Although this is a rudimentary path tracer it can still render some nice-looking scenes:

![collage](https://wintrytbs.gitlab.io/website_test/images/rtiow/collage.png)

# Building
Currently only Windows is supported.  
To compile, run the **build.bat** file. Clang is the default compiler so it needs to be available on the command line.  
```
build.bat
```
MSVC can be invoked by passing the argument "msvc" to the build file. This requires that cl.exe is available on the command line.  
MSVC build requires Visual Studio 2019 version 16.8 which support specifying conformance to ISO C11
```
build.bat msvc
```


# References

<https://raytracing.github.io/>
